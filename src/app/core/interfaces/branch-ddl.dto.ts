export interface BranchDdlDto {
  branchNum: number
  branchDesc: string
}
