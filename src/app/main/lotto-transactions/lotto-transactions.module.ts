import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { LottoTransactionsRoutingModule } from './lotto-transactions-routing.module'
import { LottoTransactionsService } from './services/lotto-transactions.service'
import { LottoTransactionsComponent } from './components/lotto-transactions.component'
import { SharedModule } from '../../shared/shared.module'


@NgModule({
  declarations: [LottoTransactionsComponent],
  imports: [
    CommonModule,
    LottoTransactionsRoutingModule,
    NgbModalModule,
    FormsModule,
    NgxDatatableModule,
    SharedModule
  ],
  providers: [
    LottoTransactionsService
  ]
})
export class LottoTransactionsModule {
}
