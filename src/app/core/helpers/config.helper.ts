import { environment } from '../../../environments/environment'

export class Config {

  public static getControllerUrl(controllerName: string, actionName?: string, isMobile: boolean = false): string {
    if (isMobile) {
      return this.getMobileApiUrl() + '/' + controllerName + (actionName ? '/' + actionName : '')
    }
    return this.getApiUrl() + '/' + controllerName + (actionName ? '/' + actionName : '')
  }

  public static getApiUrl(): string {
    return this.getServerUrl() + '/api'
  }

  public static getMobileApiUrl(): string {
    return this.getServerUrl() + '/api-mobile'
  }

  public static getServerUrl(): string {
    return environment.production ? '' : 'https://localhost:44390'
  }
}
