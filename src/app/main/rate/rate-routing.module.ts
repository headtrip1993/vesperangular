import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { AddRateComponent } from './components/add-rate/add-rate.component'
import { AllRatesComponent } from './components/all-rates/all-rates.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddRateComponent,
    data: {
      title: 'Rate',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllRatesComponent,
    data: {
      title: 'Rates',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RateRoutingModule {

}
