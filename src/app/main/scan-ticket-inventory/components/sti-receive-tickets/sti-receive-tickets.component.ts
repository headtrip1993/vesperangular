import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { forkJoin, of } from "rxjs";

import { CookieService } from "src/app/core/services/cookie.service";
import { ScanTicketInventoryService } from "../../services/scan-ticket-inventory.service";
import { StiCsvParserService } from "../../services/sti-csv-parser.service";
import { DateParserService } from "../../services/date-parser.service";
import { BranchDdlDto } from "src/app/core/interfaces/branch-ddl.dto";
import { Ticket } from "../../interfaces/ticket";

@Component({
  selector: "app-sti-receive-tickets",
  templateUrl: "./sti-receive-tickets.component.html",
  styleUrls: ["./sti-receive-tickets.component.scss"],
})
export class StiReceiveTicketsComponent implements OnInit {
  @ViewChild('file_upload') file_upload: ElementRef

  selectedBranch: BranchDdlDto;
  drForm: FormGroup;
  scanOptionForm: FormGroup;
  scanTypeForm: FormGroup;

  loadedTickets: [];
  active: number = 1;
  selectedOption: string;
  isLoading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private cookieService: CookieService,
    private scanTicketInventoryService: ScanTicketInventoryService,
    private csvParser: StiCsvParserService,
    private dateParser: DateParserService
  ) {}

  ngOnInit(): void {
    this.selectedBranch =
      JSON.parse(this.cookieService.getCookie("branch")) ?? null;

    this.drForm = this.fb.group({
      date: this.fb.control(this.dateParser.formatDate(new Date())),
    });

    this.scanOptionForm = this.fb.group({
      type: this.fb.control("upload-csv"),
    });

    this.scanTypeForm = this.fb.group({
      scan_type: this.fb.control("book", Validators.required),
      book: this.fb.control(""),
      range_from: this.fb.control(""),
      range_to: this.fb.control(""),
    });

    // fetch all received tickers
    this.onGetAllReceivedTickets();

    // subscribe to selected date
    this.drForm.controls.date.valueChanges.subscribe(date => {
      this.onGetAllReceivedTickets()
    })
  }

  onGetAllReceivedTickets() {
    this.scanTicketInventoryService
      .getAllTransferedTickets()
      .subscribe((res: any) => {
        this.loadedTickets = res.body;
      });
  }

  onUploadReceiveTickets(files) {
    if (files && files.length > 0) {
      let file: File = files.item(0);
      let reader: FileReader = new FileReader();

      reader.readAsText(file);
      reader.onload = (e) => {
        let csv: string = reader.result as string;
        let parsedCSV = this.csvParser.parseCSV(csv);
        let obervableArray = [];

        parsedCSV.forEach((data: Ticket) => {
          let payload = {
            ...data,
            branchNum: this.selectedBranch.branchNum,
            posted: false,
            transferedBySGBranch: "",
          };

          obervableArray.push(this.scanTicketInventoryService.addTransferedTicket(payload));
        });

        forkJoin(obervableArray).subscribe(() => this.onGetAllReceivedTickets())
        this.file_upload.nativeElement.value = ''
      };
    }
  }
}
