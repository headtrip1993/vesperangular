import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchRoutingModule } from './branch-routing.module';
import { AddBranchComponent } from './components/add-branch/add-branch.component';
import { AllBranchesComponent } from './components/all-branches/all-branches.component'
import { BranchHttpService } from './services/branch-http.service'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'

@NgModule({
  declarations: [AddBranchComponent, AllBranchesComponent],
  imports: [
    CommonModule,
    BranchRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    BranchHttpService
  ]
})
export class BranchModule { }
