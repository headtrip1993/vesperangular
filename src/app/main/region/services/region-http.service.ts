import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { RegionModel } from '../models/region.model'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { RegionDto } from '../interfaces/region.dto'

@Injectable()
export class RegionHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addRegion(regionModel: RegionModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Region', ''), regionModel, {observe: 'response'})
  }

  public updateRegion(regionModel: RegionModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Region', 'UpdateRegion'), regionModel, {observe: 'response'})
  }

  public getAllRegions(): Observable<HttpResponse<RegionDto[]>> {
    return this.httpClient
      .get<RegionDto[]>(Config.getControllerUrl('Region', ''), {observe: 'response'})
  }

  public deleteRegion(regionId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Region', 'DeleteRegionById'), regionId, {observe: 'response'})
  }
}
