import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../core/helpers/config.helper'

@Injectable()
export class AuthService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public login(userName: string, password: string, branchNum: string): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('User', `Login?username=${userName}&password=${password}&BranchNum=${branchNum}`),
        null, {observe: 'response'})
  }
}
