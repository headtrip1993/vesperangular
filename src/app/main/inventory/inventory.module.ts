import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AddInventoryComponent } from './components/add-inventory/add-inventory.component'
import { AllInventoryComponent } from './components/all-inventory/all-inventory.component'
import { InventoryRoutingModule } from './inventory-routing.module'
import { InventoryHttpService } from './services/inventory-http.service'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'


@NgModule({
  declarations: [AddInventoryComponent, AllInventoryComponent],
  imports: [
    CommonModule,
    InventoryRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    InventoryHttpService
  ]
})
export class InventoryModule {
}
