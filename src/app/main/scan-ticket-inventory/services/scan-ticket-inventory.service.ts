import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../../../core/helpers/config.helper'
import { Ticket } from '../interfaces/ticket';

@Injectable({
  providedIn: 'root'
})
export class ScanTicketInventoryService {

  constructor(
    private readonly http: HttpClient
  ) { }

  public getAllTransferedTickets(): Observable<any> {
    return this.http
      .get<HttpResponse<any>>(Config.getControllerUrl('OutletInstantsTransaction', ''), {observe: 'response'})
  }

  public addTransferedTicket(ticket: Ticket): Observable<any> {
    return this.http
      .post<HttpResponse<any>>(Config.getControllerUrl('OutletInstantsTransaction'), ticket, {observe: 'response'})
  }

  // public Ticket(ticket: Ticket): Observable<any> {
  //   return this.http
  //     .post<HttpResponse<any>>(Config.getControllerUrl('OutletInstantsTransaction'), ticket, {observe: 'response'})
  // }
}
