import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router'
import { Injectable } from '@angular/core'
import { CookieService } from '../services/cookie.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private readonly router: Router,
    private readonly cookieService: CookieService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const user = JSON.parse(this.cookieService.getCookie('user'))
    if (user) {
      return true
    }

    this.router.navigate(['/auth/login'])
    return false
  }

}
