import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { AreaModel } from '../models/area.model'
import { AreaDto } from '../interfaces/area.dto'
import { Injectable } from '@angular/core'

@Injectable()
export class AreaHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addArea(areaModel: AreaModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Area', ''), areaModel, {observe: 'response'})
  }

  public getAllAreas(): Observable<HttpResponse<AreaDto[]>> {
    return this.httpClient
      .get<AreaDto[]>(Config.getControllerUrl('Area', ''), {observe: 'response'})
  }

  public updateArea(areaModel: AreaModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Area', 'UpdateArea'), areaModel, {observe: 'response'})
  }

  public deleteArea(areaId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Area', 'DeleteAreaById'), areaId, {observe: 'response'})
  }
}

