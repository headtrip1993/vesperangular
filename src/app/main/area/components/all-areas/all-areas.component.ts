import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import * as dayjs from 'dayjs'
import { AreaDto } from '../../interfaces/area.dto'
import { AreaHttpService } from '../../services/area-http.service'

@Component({
  selector: 'app-all-areas',
  templateUrl: './all-areas.component.html',
  styleUrls: ['./all-areas.component.scss']
})
export class AllAreasComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public areasDTO: AreaDto[]
  public tempData: AreaDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly areaHttpService: AreaHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllAreas()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.areasDTO = this.tempData.filter(d => {
      return d.areaName.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: AreaDto): void {
    debugger
    console.log(row)
    this.router.navigate(['/area/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: AreaDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.areaHttpService.deleteArea(row.areaNum)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting area!', 'Error')
                return
              }
              this.toastrService.success('Area successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Area has been deleted.',
                'success'
              )
              this.getAllAreas()
            }, error => {
              this.toastrService.error('Error deleting area!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllAreas(): void {
    this.isLoading = true
    this.subs.add(
      this.areaHttpService.getAllAreas()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.areasDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
