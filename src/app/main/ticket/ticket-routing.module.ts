import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { AllTicketsComponent } from './components/all-tickets/all-tickets.component'
import { AddTicketComponent } from './components/add-ticket/add-ticket.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddTicketComponent,
    data: {
      title: 'Ticket',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllTicketsComponent,
    data: {
      title: 'Tickets',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TicketRoutingModule {

}
