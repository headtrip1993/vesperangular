import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CategoryRoutingModule } from './category-routing.module'
import { AddCategoryComponent } from './components/add-category/add-category.component'
import { AllCategoriesComponent } from './components/all-categories/all-categories.component'
import { ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { SharedModule } from '../../shared/shared.module'
import { CategoryHttpService } from './services/category-http.service'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'

@NgModule({
  declarations: [
    AddCategoryComponent,
    AllCategoriesComponent,
  ],
    imports: [
        CommonModule,
        CategoryRoutingModule,
        ReactiveFormsModule,
        NgbModule,
        SharedModule,
        NgxDatatableModule,
    ],
  providers: [
    CategoryHttpService
  ]
})
export class CategoryModule {
}
