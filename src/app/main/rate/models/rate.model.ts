export class RateModel {
  id?: number
  rateName: string
  rateCode: string
  rateValue: number
  userCode: string
}
