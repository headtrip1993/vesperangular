export class CompanyDto {
  companyNum: number
  companyName: string
  logo: string
}
