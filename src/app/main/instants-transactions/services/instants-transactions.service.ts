import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { InventoryDto } from '../../inventory/interfaces/inventory.dto'
import { Config } from '../../../core/helpers/config.helper'
import { InventoryItemsDto } from '../interfaces/inventory-items.dto'

@Injectable()
export class InstantsTransactionsService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public getInventoryItemsDropdown(): Observable<InventoryItemsDto[]> {
    return this.httpClient
      .get<InventoryItemsDto[]>(Config.getControllerUrl('InvItems', 'GetDDItems'))
  }

  public saveInstantTransaction(data: any): Observable<any> {
    return this.httpClient
      .post<any>(Config.getControllerUrl('Lottomain', ''), data)
  }
}
