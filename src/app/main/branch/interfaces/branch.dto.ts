export interface BranchDto {
  branchNum?: number
  branchId: string
  sgBranch: string
  inst_comp: string
  keno_comp: string
  branchDesc: string
  areaId: string
  regionId: string
  divisionId: string
  showInSearch: boolean
  upt_date: Date
  userCode: string
  cluster: number
  nGroup: number
  malls: boolean
  nLotto: number
  mallGrouping: string
  agencyNo: string
  agencynoKeno: string
  agencynoCoronis: string
  agencynoExtrema: string
  agencyMicrolot: string
  isasSortOrder: number
  isasSortOrder2: number
  classification: string
  shortname: string
}
