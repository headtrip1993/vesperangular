import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import { RegionDto } from '../../interfaces/region.dto'
import { RegionHttpService } from '../../services/region-http.service'
import * as dayjs from 'dayjs'

@Component({
  selector: 'app-all-regions',
  templateUrl: './all-regions.component.html',
  styleUrls: ['./all-regions.component.scss']
})
export class AllRegionsComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public regionsDTO: RegionDto[]
  public tempData: RegionDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly regionHttpService: RegionHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllRegions()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.regionsDTO = this.tempData.filter(d => {
      return d.regionName.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: RegionDto): void {
    console.log(row)
    this.router.navigate(['/region/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: RegionDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.regionHttpService.deleteRegion(row.regionNum)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting region!', 'Error')
                return
              }
              this.toastrService.success('Region successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Region has been deleted.',
                'success'
              )
              this.getAllRegions()
            }, error => {
              this.toastrService.error('Error deleting region!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllRegions(): void {
    this.isLoading = true
    this.subs.add(
      this.regionHttpService.getAllRegions()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.regionsDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
