import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { TicketModel } from '../models/ticket.model'
import { TicketDto } from '../interfaces/ticket.dto'

@Injectable()
export class TicketHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addTicket(ticketModel: TicketModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Ticket', ''), ticketModel, {observe: 'response'})
  }

  public getAllTickets(): Observable<HttpResponse<TicketDto[]>> {
    return this.httpClient
      .get<TicketDto[]>(Config.getControllerUrl('Ticket', ''), {observe: 'response'})
  }

  public updateTicket(ticketModel: TicketModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Ticket', 'UpdateTicket'), ticketModel, {observe: 'response'})
  }

  public deleteTicket(ticketId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Ticket', 'DeleteTicketById'), ticketId, {observe: 'response'})
  }
}
