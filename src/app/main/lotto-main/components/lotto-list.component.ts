import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import * as dayjs from 'dayjs'
import { LottoMainService } from '../services/lotto-main.service'

@Component({
  selector: 'app-all-rates',
  templateUrl: './lotto-list.component.html',
  styleUrls: ['./lotto-list.component.scss']
})
export class LottoListComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public lottoMainList: any[]
  // public tempData: any[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly lottoMainService: LottoMainService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getLottoMainList()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    // const val = event.target.value.toLowerCase()
    //
    // // filter our data
    // // update the rows
    // this.ratesDTO = this.tempData.filter(d => {
    //   return d.rateName.toString().toLowerCase().indexOf(val) !== -1 || !val
    // })
    // // Whenever the filter changes, always go back to the first page
    // this.dataTable.offset = 0
  }

  public onEdit(row: any): void {
    // console.log(row)
    // this.router.navigate(['/rate/add'], {
    //   state: {
    //     data: row
    //   }
    // })
  }

  public onDelete(row: any): void {
    // console.log(row)
    // Swal.fire({
    //   title: 'Are you sure?',
    //   text: 'You won\'t be able to revert this!',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Yes, delete it!'
    // }).then((result) => {
    //   if (result.isConfirmed) {
    //     this.subs.add(
    //       this.lottoMainService.deleteRate(row.id)
    //         .subscribe(response => {
    //           console.log(response)
    //           if (response.status !== 200) {
    //             this.toastrService.error('Error deleting rate!', 'Error')
    //             return
    //           }
    //           this.toastrService.success('Rate successfully deleted!', 'Success')
    //           Swal.fire(
    //             'Deleted!',
    //             'Rate has been deleted.',
    //             'success'
    //           )
    //           this.getAllRates()
    //         }, error => {
    //           this.toastrService.error('Error deleting rate!', 'Error')
    //         }, () => {
    //           this.isLoading = false
    //         }))
    //   }
    // })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getLottoMainList(): void {
    this.isLoading = true
    this.subs.add(
      this.lottoMainService.getAllLottoMain()
        .subscribe(response => {
          debugger
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.lottoMainList = response?.body ?? []
          // this.tempData = [...response?.body]
          console.log(response)
        }, error => {
          this.toastrService.error('Error occurred!')
          console.log(error)
        }, () => {
          this.isLoading = false
        })
    )
  }
}
