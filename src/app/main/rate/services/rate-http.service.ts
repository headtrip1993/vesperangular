import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { RateModel } from '../models/rate.model'
import { RateDto } from '../interfaces/rate.dto'

@Injectable()
export class RateHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addRate(rateModel: RateModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Rate', ''), rateModel, {observe: 'response'})
  }

  public getAllRates(): Observable<HttpResponse<RateDto[]>> {
    return this.httpClient
      .get<RateDto[]>(Config.getControllerUrl('Rate', ''), {observe: 'response'})
  }

  public updateRate(rateModel: RateModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Rate', 'UpdateRates'), rateModel, {observe: 'response'})
  }

  public deleteRate(rateId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Rate', 'DeleteRateById'), rateId, {observe: 'response'})
  }
}
