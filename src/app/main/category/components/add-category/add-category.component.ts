import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { CategoryHttpService } from '../../services/category-http.service'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public categoryFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly categoryHttpService: CategoryHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.categoryFormGroup = new FormGroup({
      itemId: new FormControl(null, []),
      name: new FormControl(null, [Validators.required]),
      newComputation: new FormControl(false, [])
    })

    if (this.isEditing) {
      this.categoryFormGroup.setValue(history.state.data)
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.categoryFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.categoryHttpService.addCategory(this.categoryFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving category!', 'Error')
              return
            }
            this.toastrService.success('Category successfully saved!', 'Success')
            this.categoryFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving category!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.categoryHttpService.updateCategory(this.categoryFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating category!', 'Error')
              return
            }
            this.toastrService.success('Category successfully updated!', 'Success')
            this.categoryFormGroup.reset()
            this.router.navigate(['/category/all'])
          }, error => {
            this.toastrService.error('Error updating category!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }
  }

}
