export class RegionModel {
  regionNum?: number
  regionId: string
  regionName: string
  showInSearch: boolean
  userCode: string
  islandGrouping: string
  islandOrder: number
}
