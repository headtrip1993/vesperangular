import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { Subscription } from 'rxjs'
import { DatatableComponent, SelectionType, TableColumn } from '@swimlane/ngx-datatable'
import { InventoryItemsDto } from '../../instants-transactions/interfaces/inventory-items.dto'
import { CookieService } from '../../../core/services/cookie.service'
import { LottoTransactionsService } from '../services/lotto-transactions.service'
import { BranchDdlDto } from '../../../core/interfaces/branch-ddl.dto'
import { BranchDto } from '../../branch/interfaces/branch.dto'
import { Router } from '@angular/router'

@Component({
  selector: 'app-instants-transactions',
  templateUrl: './lotto-transactions.component.html',
  styleUrls: ['./lotto-transactions.component.scss']
})
export class LottoTransactionsComponent implements OnInit, OnDestroy {

  @ViewChild('modal')
  public modal: TemplateRef<any>
  @ViewChild('datatableComponent')
  public dataTableComponent: DatatableComponent
  @ViewChild('datatableComponentMain')
  public dataTableComponentMain: DatatableComponent

  public isLoading: boolean
  public branchName: string
  public inventoryItems: InventoryItemsDto[] = null
  public selectedInventoryItem: InventoryItemsDto = null
  public columns: TableColumn[] = []
  public mainColumn: TableColumn[] = []

  public details: any[] = []
  public mainRows: any[] = []
  public selectedRows: any[] = []
  public selectionType: SelectionType = SelectionType.checkbox
  public isSelectAllRowsOnPage = false
  public payouts: number
  public stampTax = 0.0
  public transactionId = 'XXXXXXXXXX'

  public beginningCash = 0.0
  public cashSales = 0.0
  public endingCash = 0.0
  public cashDeposit = 0.0

  public overage = 0.0
  public partial = 0.0
  public remaining = 0.0

  public selectedDate
  public branchId = '0'
  public lastModifiedBy
  public dateLastModified

  private subs: Subscription = new Subscription()

  public detailModel = {
    itemCode: null,
    sales: null,
    commission: null,
    cancels: null,
  }

  constructor(
    public readonly ngbModal: NgbModal,
    private readonly cookieService: CookieService,
    private readonly lottoTransactionsService: LottoTransactionsService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.branchName = JSON.parse(this.cookieService.getCookie('branch')).branchDesc
    this.getInventoryItemsDropdown()
    this.mainColumn = [
      {
        name: 'Sales',
        prop: 'sales'
      },
      {
        name: 'DST',
        prop: 'dst'
      },
      {
        name: 'Cancels',
        prop: 'cancels'
      },
      {
        name: 'Payouts',
        prop: 'payouts'
      },
      {
        name: 'Net Sales',
        prop: 'netSales'
      },
      {
        name: 'Cash Sales',
        prop: 'cashSales'
      },
      {
        name: 'Commission',
        prop: 'commission'
      },
      {
        name: 'WH Tax',
        prop: 'whTax'
      },
      {
        name: 'Net Comm',
        prop: 'netComm'
      },
      {
        name: 'Balance',
        prop: 'balance'
      },
      {
        name: 'Adjustment',
        prop: 'adjustment'
      },
      {
        name: 'Net Due',
        prop: 'netDue'
      }
    ]

    this.columns = [
      {
        width: 30,
        sortable: false,
        canAutoResize: false,
        draggable: false,
        resizeable: false,
        headerCheckboxable: true,
        checkboxable: true
      },
      {
        name: 'Item Code',
        prop: 'itemCode'
      },
      {
        name: 'Sales',
        prop: 'sales'
      },
      {
        name: 'Commission',
        prop: 'commission'
      },
      {
        name: 'Cancels',
        prop: 'cancels'
      }
    ]

    this.branchId = JSON.parse(this.cookieService.getCookie('branch')).branchNum ?? 0
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get rows(): any[] {
    return this.details.map(a => {
      return {
        ...a,
        itemCode: a.itemCode.itemCode
      }
    })
  }

  public get mainTableRows(): any[] {
    return this.mainRows.map(a => {
      return {...a}
    })
  }

  public get showSumSales(): number {
    return this.sumCancelsSales()[0]
  }

  public get showSumCancels(): number {
    return this.sumCancelsSales()[1]
  }

  public onAdd(): void {
    if (this.selectedDate) {
      this.ngbModal.open(this.modal, {
        size: 'xl',
        centered: true,
        backdrop: 'static'
      })
    }
  }

  public onTransactionDateChange(value: any): void {
    console.log(value.target.value)
    this.getTransactionDetails(value.target.value)
  }

  public onMainSave(): void {
    console.log(JSON.parse(this.cookieService.getCookie('user'))[0].userCode)
    this.isLoading = true
    const branch: BranchDdlDto = JSON.parse(this.cookieService.getCookie('branch'))

    const temp = {
      transcationid: 0,
      transxn_id: 'string',
      branchId: branch.branchNum.toString(),
      branchDesc: branch.branchDesc,
      tranDate: '2021-03-03T15:58:51.163Z',
      tranTime: 'string',
      beginningCash: 0,
      cashSales: this.mainRows[0].cashSales,
      cashDeposit: 0,
      otherCashDep: 0,
      endingCash: 0,
      overAge: 0,
      upt_Date: '2021-03-03T15:58:51.163Z',
      userCode: JSON.parse(this.cookieService.getCookie('user')).username,
      forPosting: true,
      posted: true,
      postedUser: 'string',
      postedDate: '2021-03-03T15:58:51.163Z',
      accountId: 'string',
      deptId: 'string',
      subDeptId: 'string',
      category: 'string',
      cashRemaining: 0,
      cashPartial: 0,
      deptTime: '2021-03-03T15:58:51.163Z',
      remarks: 'string',
      renTime: 'string',
      pcsoTime: 'string',
      modifiedBy: 'string',
      modifiedDate: '2021-03-03T15:58:51.163Z',
      ds: 0,
      lottoDtls: this.rows.map(a => {
        return {
          detailId: 0,
          transcationId: 0,
          transxn_Id: 'string',
          sales: a.sales,
          cancels: a.cancels,
          payout: 0,
          netSales: 0,
          cashSales: 0,
          commission: a.commission,
          whTax: 0,
          netCommission: 0,
          balance: 0,
          overage: 0,
          netDue: 0,
          netDueValidate: 0,
          begBal: 0,
          dst: 0
        }
      })
    }

    const mainObj = {
      branchId: branch.branchNum.toString(),
      branchDesc: branch.branchDesc,
      beginningCash: this.beginningCash,
      cashSales: this.mainRows[0].cashSales,
      cashDeposit: this.cashDeposit,
      otherCashDep: 0,
      cashPartial: this.partial ?? 0.0,
      cashRemaining: this.remaining ?? 0.0,
      overAge: this.overage ?? 0.0,
      tranDate: this.selectedDate,
      endingCash: this.endingCash,
      userCode: JSON.parse(this.cookieService.getCookie('user'))[0].userCode ?? '0',
      lottoDtls: this.rows
    }

    this.subs.add(
      this.lottoTransactionsService.saveInstantTransaction(mainObj)
        .subscribe(response => {
          console.log(response)

          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/lotto-transactions/transactions'])
          })
          this.isLoading = false
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }

  public onMainComputeTax(): void {
    this.cashSales = this.mainTableRows[0].cashSales
    this.remaining = this.cashSales + this.stampTax - this.partial ?? 0.0
    this.endingCash = this.beginningCash - this.cashDeposit + this.remaining + this.overage
    this.stampTax = this.mainTableRows[0].stampTax ?? 0.0
  }

  public onSaveModal(): void {
    this.mainRows.push(
      {
        sales: this.sumCancelsSales()[1],
        dst: 0,
        cancels: this.sumCancelsSales()[0],
        payouts: this.payouts ?? 0,
        netSales: 0,
        cashSales: this.sumCancelsSales()[1],
        commission: (this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) * this.stampTax ?? 0,
        whTax: 0,
        netComm: ((this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) * this.stampTax ?? 0) - this.stampTax ?? 0,
        balance: this.sumCancelsSales()[1] - this.sumCancelsSales()[0] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) + this.stampTax ?? 0 + 0,
        adjustment: 0,
        netDue: (this.sumCancelsSales()[1] - this.sumCancelsSales()[0] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) + this.stampTax ?? 0 + 0) + 0
      }
    )

    this.ngbModal.dismissAll()
  }

  public onSelect({selected}): void {
    console.log('Select Event', selected, this.selectedRows)

    this.selectedRows.splice(0, this.selectedRows.length)
    this.selectedRows.push(...selected)
  }

  public onBackModal(): void {
    this.ngbModal.dismissAll()
  }

  public onDetailAdd(): void {
    this.details.push(this.detailModel)
    this.detailModel = {
      itemCode: null,
      sales: null,
      commission: null,
      cancels: null,
    }
  }

  public sumCancelsSales(): number[] {
    let cancelsSum = 0
    let salesSum = 0

    this.details.forEach(d => {
      cancelsSum += d.cancels
    })

    this.details.forEach(d => {
      salesSum += d.sales
    })

    return [salesSum, cancelsSum]
  }

  private getInventoryItemsDropdown(): void {
    this.isLoading = true
    this.subs.add(
      this.lottoTransactionsService
        .getInventoryItemsDropdown()
        .subscribe(response => {
          this.inventoryItems = response
          this.isLoading = false
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }

  private getTransactionDetails(date: string): void {
    this.isLoading = true

    const branch: BranchDdlDto = JSON.parse(this.cookieService?.getCookie('branch'))

    console.log(branch)

    this.subs.add(
      this.lottoTransactionsService.getTransactionDetails(date, branch.branchNum.toString() ?? '0')
        .subscribe(response => {
          console.log(response)
          if (!response || response.length === 0) {
            this.mainRows = []
            this.details = []
            this.stampTax = 0
            this.payouts = 0
            this.transactionId = 'XXXXXXXXXX'
            this.beginningCash = 0.0
            this.cashSales = 0.0
            this.cashDeposit = 0.0
            this.endingCash = 0.0
            this.lastModifiedBy = null
            this.dateLastModified = null
            return
          }
          if (!response[0].lottoDtls) {
            this.mainRows = []
            this.details = []
            this.stampTax = 0
            this.payouts = 0
            this.transactionId = 'XXXXXXXXXX'
            this.beginningCash = response[0].endingCash ?? 0.0
            this.cashSales = 0.0
            this.cashDeposit = 0.0
            this.endingCash = 0.0
            this.lastModifiedBy = null
            this.dateLastModified = null
            return
          }

          this.lastModifiedBy = response[0].modifiedBy ?? null
          this.dateLastModified = response[0].modifiedDate ?? null

          this.details = response[0].lottoDtls.map(l => {
            return {
              itemCode: {itemCode: l.itemCode},
              sales: l.sales,
              commission: l.commission,
              cancels: l.cancels
            }
          })

          this.stampTax = response[0]?.lottoDtls[0]?.dst ?? 0.0
          this.payouts = response[0]?.lottoDtls[0]?.payout ?? 0.0
          this.transactionId = response[0]?.transcationid ?? 0.0
          this.beginningCash = response[0]?.beginningCash ?? 0.0
          this.cashSales = response[0]?.cashSales ?? 0.0
          this.cashDeposit = response[0]?.cashDeposit ?? 0.0
          this.endingCash = response[0]?.endingCash ?? 0.0
          this.mainRows = response.map(r => {
            return {
              sales: this.sumCancelsSales()[0],
              dst: 0,
              cancels: this.sumCancelsSales()[1],
              payouts: this.payouts ?? 0,
              netSales: 0,
              cashSales: this.sumCancelsSales()[0],
              commission: (this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) * this.stampTax ?? 0,
              whTax: 0,
              netComm: ((this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) * this.stampTax ?? 0) - this.stampTax ?? 0,
              balance: this.sumCancelsSales()[0] - this.sumCancelsSales()[1] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) + this.stampTax ?? 0 + 0,
              adjustment: 0,
              netDue: (this.sumCancelsSales()[0] - this.sumCancelsSales()[1] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) + this.stampTax ?? 0 + 0) + 0
            }
          })
          this.onMainComputeTax()
        }, error => {
          alert(error)
        }, () => {
          this.isLoading = false
        })
    )
  }
}
