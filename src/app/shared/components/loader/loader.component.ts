import { Component, OnInit, OnDestroy, Input } from '@angular/core'

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {

  @Input()
  public show = true

  constructor() {
    setTimeout(() => {
      this.show = false
    }, 1000)
  }

  ngOnInit(): void {
    this.show = true
  }

  ngOnDestroy(): void {
  }

}
