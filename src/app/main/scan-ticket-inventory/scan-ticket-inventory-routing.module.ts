import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ScanTicketInventoryComponent } from "./components/scan-ticket-inventory/scan-ticket-inventory.component";
import { StiReceiveTicketsComponent } from "./components/sti-receive-tickets/sti-receive-tickets.component";
import { StiTransferTicketsComponent } from "./components/sti-transfer-tickets/sti-transfer-tickets.component";

const routes: Routes = [
  {
    path: "",
    component: ScanTicketInventoryComponent,
    data: {
      title: "Scan Ticket Inventory",
      breadcrumb: "",
    },
    children: [
      {
        path: "",
        redirectTo: "receive-tickets",
        pathMatch: "full",
      },
      {
        path: "receive-tickets",
        component: StiReceiveTicketsComponent,
        data: { title: "Receive Tickets", breadcrumb: "" },
      },
      {
        path: "transfer-tickets",
        component: StiTransferTicketsComponent,
        data: { title: "Transfer Tickets", breadcrumb: "" },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScanTicketInventoryRoutingModule {}
