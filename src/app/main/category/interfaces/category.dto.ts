export interface CategoryDto {
  itemId: number,
  name: string,
  newComputation: boolean
}
