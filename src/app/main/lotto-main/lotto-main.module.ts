import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { LottoMainRoutingModule } from './lotto-main-routing.module'
import { LottoListComponent } from './components/lotto-list.component'
import { LottoMainService } from './services/lotto-main.service'
import { SharedModule } from '../../shared/shared.module'


@NgModule({
  declarations: [LottoListComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    FormsModule,
    NgxDatatableModule,
    LottoMainRoutingModule,
    SharedModule
  ],
  providers: [
    LottoMainService
  ]
})
export class LottoMainModule {
}
