export class LottoVariantModel {
  itemNum?: number
  itemID: number
  itemDesc: string
  itemCode: string
  commRate: number
  showInSearch: boolean
  showInReport: boolean
  unitPrice: number
  userCode: string
  lottoGame: boolean
}
