import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import * as dayjs from 'dayjs'
import { LottoVariantDto } from '../../interfaces/lotto-variant.dto'
import { LottoVariantHttpService } from '../../services/lotto-variant-http.service'

@Component({
  selector: 'app-all-lotto-variants',
  templateUrl: './all-lotto-variants.component.html',
  styleUrls: ['./all-lotto-variants.component.scss']
})
export class AllLottoVariantsComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public lottoVariantsDTO: LottoVariantDto[]
  public tempData: LottoVariantDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly lottoVariantHttpService: LottoVariantHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllLottoVariants()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.lottoVariantsDTO = this.tempData.filter(d => {
      return d.itemCode.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: LottoVariantDto): void {
    debugger
    console.log(row)
    this.router.navigate(['/lotto-variant/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: LottoVariantDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.lottoVariantHttpService.deleteLottoVariant(row.itemNum)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting lotto variant!', 'Error')
                return
              }
              this.toastrService.success('Lotto Variant successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Lotto Variant has been deleted.',
                'success'
              )
              this.getAllLottoVariants()
            }, error => {
              this.toastrService.error('Error deleting lotto variant!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllLottoVariants(): void {
    this.isLoading = true
    this.subs.add(
      this.lottoVariantHttpService.getAllLottoVariants()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.lottoVariantsDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
