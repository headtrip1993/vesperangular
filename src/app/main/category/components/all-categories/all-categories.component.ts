import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { Subscription } from 'rxjs'
import { CategoryHttpService } from '../../services/category-http.service'
import { ToastrService } from 'ngx-toastr'
import { CategoryDto } from '../../interfaces/category.dto'
import { DatatableComponent, TableColumn } from '@swimlane/ngx-datatable'
import { Router } from '@angular/router'

import Swal from 'sweetalert2'

@Component({
  selector: 'app-all-categories',
  templateUrl: './all-categories.component.html',
  styleUrls: ['./all-categories.component.scss']
})
export class AllCategoriesComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public categoriesDTO: CategoryDto[]
  public tempData: CategoryDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly categoryHttpService: CategoryHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllCategories()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.categoriesDTO = this.tempData.filter(d => {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: CategoryDto): void {
    console.log(row)
    this.router.navigate(['/category/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: CategoryDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.categoryHttpService.deleteCategory(row.itemId)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting category!', 'Error')
                return
              }
              this.toastrService.success('Category successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Category has been deleted.',
                'success'
              )
              this.getAllCategories()
            }, error => {
              this.toastrService.error('Error deleting category!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  private getAllCategories(): void {
    this.isLoading = true
    this.subs.add(
      this.categoryHttpService.getAllCategories()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.categoriesDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }

}
