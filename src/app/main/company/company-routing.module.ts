import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AddCompanyComponent } from './companies/add-company/add-company.component'
import { AllCompaniesComponent } from './companies/all-companies/all-companies.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddCompanyComponent,
    data: {
      title: 'Company',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllCompaniesComponent,
    data: {
      title: 'Companies',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CompanyRoutingModule {

}
