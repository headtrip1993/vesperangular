import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { BranchDdlDto } from '../interfaces/branch-ddl.dto'
import { Config } from '../helpers/config.helper'
import { UserGroupDdlDto } from '../interfaces/user-group-ddl.dto'

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public getBranches(): Observable<BranchDdlDto[]> {
    return this.httpClient
      .get<BranchDdlDto[]>(Config.getControllerUrl('Branch', 'GetDDBranchs'))
  }

  public getUserGroups(): Observable<UserGroupDdlDto[]> {
    return this.httpClient
      .get<UserGroupDdlDto[]>(Config.getControllerUrl('User', 'GetDDUserGroup'))
  }
}
