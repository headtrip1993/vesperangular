import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: 'category',
    loadChildren: () => import('./category/category.module').then(c => c.CategoryModule),
    data: {
      breadcrumb: 'Categories',
      title: 'Categories'
    }
  },
  {
    path: 'branch',
    loadChildren: () => import('./branch/branch.module').then(b => b.BranchModule),
    data: {
      breadcrumb: 'Branches',
      title: 'Branches'
    }
  },
  {
    path: 'region',
    loadChildren: () => import('./region/region.module').then(r => r.RegionModule),
    data: {
      breadcrumb: 'Regions',
      title: 'Regions'
    }
  },
  {
    path: 'company',
    loadChildren: () => import('./company/company.module').then(c => c.CompanyModule),
    data: {
      breadcrumb: 'Companies',
      title: 'Companies'
    }
  },
  {
    path: 'ticket',
    loadChildren: () => import('./ticket/ticket.module').then(t => t.TicketModule),
    data: {
      breadcrumb: 'Tickets',
      title: 'Tickets'
    }
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then(u => u.UserModule),
    data: {
      breadcrumb: 'Users',
      title: 'Users'
    }
  },
  {
    path: 'rate',
    loadChildren: () => import('./rate/rate.module').then(r => r.RateModule),
    data: {
      breadcrumb: 'Rates',
      title: 'Rates'
    }
  },
  {
    path: 'inventory',
    loadChildren: () => import('./inventory/inventory.module').then(i => i.InventoryModule),
    data: {
      breadcrumb: 'Inventory',
      title: 'Inventory'
    }
  },
  {
    path: 'area',
    loadChildren: () => import('./area/area.module').then(a => a.AreaModule),
    data: {
      breadcrumb: 'Area',
      title: 'Area'
    }
  },
  {
    path: 'lotto-variant',
    loadChildren: () => import('./lotto-variant/lotto-variant.module').then(lv => lv.LottoVariantModule),
    data: {
      breadcrumb: 'Lotto Variant',
      title: 'Lotto Variant'
    }
  },
  {
    path: 'instants-transactions',
    loadChildren: () => import('./instants-transactions/instants-transactions.module').then(it => it.InstantsTransactionsModule),
    data: {
      breadcrumb: 'Instants Transactions',
      title: 'Instants Transactions'
    }
  },
  {
    path: 'lotto-transactions',
    loadChildren: () => import('./lotto-transactions/lotto-transactions.module').then(it => it.LottoTransactionsModule),
    data: {
      breadcrumb: 'Lotto Transactions',
      title: 'Lotto Transactions'
    }
  },
  {
    path: 'keno-transactions',
    loadChildren: () => import('./keno-transactions/keno-transactions.module').then(it => it.KenoTransactionsModule),
    data: {
      breadcrumb: 'Keno Transactions',
      title: 'Keno Transactions'
    }
  },
  {
    path: 'lotto-main',
    loadChildren: () => import('./lotto-main/lotto-main.module').then(it => it.LottoMainModule),
    data: {
      breadcrumb: 'Lotto Transactions',
      title: 'Lotto Transactions'
    }
  },
  {
    path: 'keno-main',
    loadChildren: () => import('./keno-main/keno-main.module').then(it => it.KenoMainModule),
    data: {
      breadcrumb: 'Keno Transactions',
      title: 'Keno Transactions'
    }
  },
  {
    path: 'scan-ticket-inventory',
    loadChildren: () => import('./scan-ticket-inventory/scan-ticket-inventory.module').then(it => it.ScanTicketInventoryModule),
    data: {
      breadcrumb: 'Scan Ticket Inventory',
      title: 'Scan Ticket Inventory'
    }
  }

]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MainRoutingModule {

}
