import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { InventoryItemsDto } from '../../instants-transactions/interfaces/inventory-items.dto'

@Injectable()
export class KenoTransactionsService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public getInventoryItemsDropdown(): Observable<InventoryItemsDto[]> {
    return this.httpClient
      .get<InventoryItemsDto[]>(Config.getControllerUrl('InvItems', 'GetDDItems'))
  }

  public saveInstantTransaction(data: any): Observable<any> {
    return this.httpClient
      .post<any>(Config.getControllerUrl('Keno', ''), data)
  }

  public getTransactionByDate(date: string, branchId: string): Observable<any> {
    return this.httpClient
      .get<any>(Config.getControllerUrl(`Keno?dateTime=${date}&BranchId=${branchId}`))
  }
}
