import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { LottoTransactionsComponent } from './components/lotto-transactions.component'

const routes: Routes = [
  {
    path: 'transactions',
    component: LottoTransactionsComponent,
    data: {
      title: 'Lotto Transactions',
      breadcrumb: ''
    },
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LottoTransactionsRoutingModule {

}
