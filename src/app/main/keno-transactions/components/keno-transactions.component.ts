import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { Subscription } from 'rxjs'
import { DatatableComponent, SelectionType, TableColumn } from '@swimlane/ngx-datatable'
import { InventoryItemsDto } from '../../instants-transactions/interfaces/inventory-items.dto'
import { CookieService } from '../../../core/services/cookie.service'
import { BranchDdlDto } from '../../../core/interfaces/branch-ddl.dto'
import { KenoTransactionsService } from '../services/keno-transactions.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-instants-transactions',
  templateUrl: './keno-transactions.component.html',
  styleUrls: ['./keno-transactions.component.scss']
})
export class KenoTransactionsComponent implements OnInit, OnDestroy {

  @ViewChild('modal')
  public modal: TemplateRef<any>
  @ViewChild('datatableComponent')
  public dataTableComponent: DatatableComponent
  @ViewChild('datatableComponentMain')
  public dataTableComponentMain: DatatableComponent

  public isLoading: boolean
  public branchName: string
  public inventoryItems: InventoryItemsDto[] = null
  public selectedInventoryItem: InventoryItemsDto = null
  public columns: TableColumn[] = []
  public mainColumn: TableColumn[] = []

  public details: any[] = []
  public mainRows: any[] = []
  public selectedRows: any[] = []
  public selectionType: SelectionType = SelectionType.checkbox
  public isSelectAllRowsOnPage = false
  public payouts: number
  public stampTax = 0.00

  public branchId
  public selectedDate

  public beginningCash = 0.0
  public cashSales = 0.0
  public remaining = 0.0
  public endingCash = 0.0
  public cashDeposit = 0.0
  public overage = 0.0
  public partial = 0.0
  public transactionId = 'XXXXXXXXXX'

  public lastModifiedBy
  public dateLastModified

  private subs: Subscription = new Subscription()

  public detailModel = {
    itemCode: null,
    sales: null,
    payouts: null,
    cancels: null,
  }

  public detailB = {
    netSales: this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0,
    cashSales: (this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0) - this.detailModel.payouts,
    commission: (this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0) - 0,
    whTax: 0,
    netCommission: 0,
    tax: 0,
    balance: 0,
    netDueToPCSO: 0,
  }

  constructor(
    public readonly ngbModal: NgbModal,
    private readonly cookieService: CookieService,
    private readonly kenoTransactionsService: KenoTransactionsService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.branchName = JSON.parse(this.cookieService.getCookie('branch')).branchDesc
    this.getInventoryItemsDropdown()

    this.mainColumn = [
      {
        name: 'Sales',
        prop: 'sales'
      },
      {
        name: 'DST',
        prop: 'dst'
      },
      {
        name: 'Cancels',
        prop: 'cancels'
      },
      {
        name: 'Payouts',
        prop: 'payouts'
      },
      {
        name: 'Net Sales',
        prop: 'netSales'
      },
      {
        name: 'Cash Sales',
        prop: 'cashSales'
      },
      {
        name: 'Commission',
        prop: 'commission'
      },
      {
        name: 'WH Tax',
        prop: 'whTax'
      },
      {
        name: 'Net Comm',
        prop: 'netComm'
      },
      {
        name: 'Balance',
        prop: 'balance'
      },
      {
        name: 'Adjustment',
        prop: 'adjustment'
      },
      {
        name: 'Net Due',
        prop: 'netDue'
      }
    ]

    this.columns = [
      {
        width: 30,
        sortable: false,
        canAutoResize: false,
        draggable: false,
        resizeable: false,
        headerCheckboxable: true,
        checkboxable: true
      },
      {
        name: 'Item Code',
        prop: 'itemCode'
      },
      {
        name: 'Sales',
        prop: 'sales'
      },
      {
        name: 'Commission',
        prop: 'commission'
      },
      {
        name: 'Cancels',
        prop: 'cancels'
      }
    ]

    this.branchId = JSON.parse(this.cookieService.getCookie('branch')).branchNum ?? '0'
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get rows(): any[] {
    return this.details.map(a => {
      return {
        ...a,
        itemCode: a.itemCode.itemCode
      }
    })
  }

  public get mainTableRows(): any[] {
    return this.mainRows.map(a => {
      return {...a}
    })
  }

  public onAdd(): void {
    if (this.selectedDate) {
      this.ngbModal.open(this.modal, {
        size: 'xl',
        centered: true,
        backdrop: 'static'
      })
    }
  }

  public onMainSave(): void {
    debugger
    this.isLoading = true
    const branch: BranchDdlDto = JSON.parse(this.cookieService.getCookie('branch'))

    const temp = {
      transcationid: 0,
      transxn_id: 'string',
      branchId: branch.branchNum.toString(),
      branchDesc: branch.branchDesc,
      tranDate: '2021-03-03T15:58:51.163Z',
      tranTime: 'string',
      beginningCash: 0,
      cashSales: this.mainRows[0].cashSales,
      cashDeposit: 0,
      otherCashDep: 0,
      endingCash: 0,
      overAge: 0,
      upt_Date: '2021-03-03T15:58:51.163Z',
      userCode: JSON.parse(this.cookieService.getCookie('user'))[0].userCode ?? '0',
      forPosting: true,
      posted: true,
      postedUser: 'string',
      postedDate: '2021-03-03T15:58:51.163Z',
      accountId: 'string',
      deptId: 'string',
      subDeptId: 'string',
      category: 'string',
      cashRemaining: 0,
      cashPartial: 0,
      deptTime: '2021-03-03T15:58:51.163Z',
      remarks: 'string',
      renTime: 'string',
      pcsoTime: 'string',
      modifiedBy: 'string',
      modifiedDate: '2021-03-03T15:58:51.163Z',
      ds: 0,
      kenoDtls: this.rows.map(a => {
        return {
          detailId: 0,
          transcationId: 0,
          transxn_Id: 'string',
          sales: a.sales,
          cancels: a.cancels,
          payout: 0,
          netSales: 0,
          cashSales: 0,
          commission: a.commission,
          whTax: 0,
          netCommission: 0,
          balance: 0,
          overage: 0,
          netDue: 0,
          netDueValidate: 0,
          begBal: 0,
          dst: 0
        }
      })
    }

    const mainObj = {
      branchId: branch.branchNum.toString(),
      branchDesc: branch.branchDesc,
      beginningCash: this.beginningCash,
      cashSales: this.detailB.cashSales,
      cashDeposit: this.cashDeposit,
      otherCashDep: 0,
      cashPartial: this.partial ?? 0.0,
      cashRemaining: this.remaining ?? 0.0,
      overAge: this.overage ?? 0.0,
      tranDate: this.selectedDate,
      endingCash: this.endingCash,
      userCode: JSON.parse(this.cookieService.getCookie('user'))[0].userCode ?? '0',
      kenoDtls: [
        {
          payout: this.detailModel.payouts,
          cancels: this.detailModel.cancels,
          sales: this.detailModel.sales,
          ...this.detailB
        }
      ]
    }

    this.subs.add(
      this.kenoTransactionsService.saveInstantTransaction(mainObj)
        .subscribe(response => {
          console.log(response)
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/keno-transactions/transactions'])
          })
          this.isLoading = false
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }

  public onSaveModal(): void {
    this.mainRows.push(
      {
        sales: this.detailModel.sales,
        dst: 0,
        cancels: this.detailModel.cancels,
        payout: this.detailModel.payouts,
        netSales: 0,
        cashSales: this.detailB.cashSales,
        commission: this.detailB.commission,
        whTax: this.detailB.whTax,
        netComm: this.detailB.netCommission,
        balance: this.detailB.balance,
        netDue: this.detailB.netDueToPCSO
      }
    )

    this.beginningCash = 0.0
    this.cashSales = this.detailB.cashSales
    this.remaining = this.cashSales + this.stampTax - this.partial ?? 0.0
    this.endingCash = this.beginningCash - this.cashDeposit + this.remaining + this.overage

    this.ngbModal.dismissAll()
  }

  public onSelect({selected}): void {
    console.log('Select Event', selected, this.selectedRows)

    this.selectedRows.splice(0, this.selectedRows.length)
    this.selectedRows.push(...selected)
  }

  public onTransactionDateChange(value: any): void {
    console.log(value)
    this.getTransactionDetails(value)
  }

  public onBackModal(): void {
    this.ngbModal.dismissAll()
  }

  public onMainComputeTax(): void {
    this.beginningCash = 0.0
    this.cashSales = this.detailB.cashSales
    this.remaining = this.cashSales + this.stampTax - this.partial ?? 0.0
    this.endingCash = this.beginningCash - this.cashDeposit + this.remaining + this.overage
  }

  public onDetailAdd(): void {
    this.details.push(this.detailModel)
    this.detailModel = {
      itemCode: null,
      sales: null,
      cancels: null,
      payouts: null,
    }
  }

  public sumCancelsSales(): number[] {
    let cancelsSum = 0
    let salesSum = 0

    this.details.forEach(d => {
      cancelsSum += d.cancels
    })

    this.details.forEach(d => {
      salesSum += d.sales
    })

    return [salesSum, cancelsSum]
  }

  public computeTax(): void {
    this.detailB = {
      netSales: this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0,
      cashSales: (this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0) - this.detailModel.payouts,
      commission: (this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0) - 0,
      whTax: 0,
      netCommission: 0,
      tax: 0,
      balance: ((this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0) - this.detailModel.payouts) - 0 + this.stampTax,
      netDueToPCSO: ((this.detailModel.sales ?? 0 - this.detailModel.cancels ?? 0) - this.detailModel.payouts) - 0 + this.stampTax,
    }
  }

  private getInventoryItemsDropdown(): void {
    this.isLoading = true
    this.subs.add(
      this.kenoTransactionsService
        .getInventoryItemsDropdown()
        .subscribe(response => {
          this.inventoryItems = response
          this.isLoading = false
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }

  private getTransactionDetails(date: string): void {
    this.isLoading = true

    const branch: BranchDdlDto = JSON.parse(this.cookieService?.getCookie('branch'))

    console.log(branch)

    this.subs.add(
      this.kenoTransactionsService.getTransactionByDate(date, branch.branchNum.toString() ?? '0')
        .subscribe(response => {
          console.log(response)
          if (!response || response.length === 0) {
            this.mainRows = []
            this.details = []
            this.stampTax = 0
            this.payouts = 0
            this.transactionId = 'XXXXXXXXXX'
            this.beginningCash = 0.0
            this.cashSales = 0.0
            this.cashDeposit = 0.0
            this.endingCash = 0.0
            this.lastModifiedBy = null
            this.dateLastModified = null
            return
          }
          if (!response[0].kenoDtls) {
            this.mainRows = []
            this.details = []
            this.stampTax = 0
            this.payouts = 0
            this.transactionId = 'XXXXXXXXXX'
            this.beginningCash = response[0].endingCash ?? 0.0
            this.cashSales = 0.0
            this.cashDeposit = 0.0
            this.endingCash = 0.0
            this.lastModifiedBy = null
            this.dateLastModified = null
            this.detailModel = {
              cancels: 0,
              payouts: 0,
              sales: 0,
              itemCode: 0
            }
            this.computeTax()
            return
          }

          this.lastModifiedBy = response[0].modifiedBy ?? null
          this.dateLastModified = response[0].modifiedDate ?? null

          this.detailModel.payouts = response[0].kenoDtls[0].payout
          this.detailModel.cancels = response[0].kenoDtls[0].cancels
          this.detailModel.sales = response[0].kenoDtls[0].sales

          this.details = response[0].kenoDtls.map(l => {
            return {
              sales: l.sales,
              payout: l.payout,
              cancels: l.cancels
            }
          })

          this.stampTax = response[0]?.kenoDtls[0]?.dst ?? 0.0
          this.payouts = response[0]?.kenoDtls[0]?.payout ?? 0.0
          this.transactionId = response[0]?.transcationid ?? 0.0
          this.beginningCash = response[0]?.beginningCash ?? 0.0
          this.cashSales = response[0]?.cashSales ?? 0.0
          this.cashDeposit = response[0]?.cashDeposit ?? 0.0
          this.endingCash = response[0]?.endingCash ?? 0.0
          this.mainRows = response.map(r => {
            return {
              sales: this.sumCancelsSales()[0],
              dst: 0,
              cancels: this.sumCancelsSales()[1],
              payouts: this.payouts ?? 0,
              netSales: 0,
              cashSales: this.sumCancelsSales()[0],
              commission: (this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) * this.stampTax ?? 0,
              whTax: 0,
              netComm: ((this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) * this.stampTax ?? 0) - this.stampTax ?? 0,
              balance: this.sumCancelsSales()[0] - this.sumCancelsSales()[1] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) + this.stampTax ?? 0 + 0,
              adjustment: 0,
              netDue: (this.sumCancelsSales()[0] - this.sumCancelsSales()[1] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[0] - this.sumCancelsSales()[1]) + this.stampTax ?? 0 + 0) + 0
            }
          })

          this.computeTax()
        }, error => {
          alert(error)
        }, () => {
          this.isLoading = false
        })
    )
  }

}
