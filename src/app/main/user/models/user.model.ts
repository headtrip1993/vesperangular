export class UserModel {
  empNumber?: number
  fullName: string
  firstName: string
  lastName: string
  mInitial: string
  username: string
  password: string
  group: string
  secLevel: string
  stationCode: string
  userCode: string
  bYear: string
  mName: string
  favFood: string
  temp0: boolean
  inActive: string
  transferred: string
}
