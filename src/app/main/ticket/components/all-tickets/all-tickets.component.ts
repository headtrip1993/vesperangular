import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from "sweetalert2"
import * as dayjs from 'dayjs'
import { TicketDto } from '../../interfaces/ticket.dto'
import { TicketHttpService } from '../../services/ticket-http.service'

@Component({
  selector: 'app-all-tickets',
  templateUrl: './all-tickets.component.html',
  styleUrls: ['./all-tickets.component.scss']
})
export class AllTicketsComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public ticketsDTO: TicketDto[]
  public tempData: TicketDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly ticketHttpService: TicketHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllTickets()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.ticketsDTO = this.tempData.filter(d => {
      return d.quantity.toString().toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: TicketDto): void {
    console.log(row)
    this.router.navigate(['/ticket/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: TicketDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.ticketHttpService.deleteTicket(row.ticketsId)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting ticket!', 'Error')
                return
              }
              this.toastrService.success('Ticket successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Ticket has been deleted.',
                'success'
              )
              this.getAllTickets()
            }, error => {
              this.toastrService.error('Error deleting ticket!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllTickets(): void {
    this.isLoading = true
    this.subs.add(
      this.ticketHttpService.getAllTickets()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.ticketsDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
