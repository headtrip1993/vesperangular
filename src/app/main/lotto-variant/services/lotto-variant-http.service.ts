import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { LottoVariantModel } from '../models/lotto-variant.model'
import { LottoVariantDto } from '../interfaces/lotto-variant.dto'

@Injectable()
export class LottoVariantHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addLottoVariant(lottoVariantModel: LottoVariantModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('LottoVariants', ''), lottoVariantModel, {observe: 'response'})
  }

  public getAllLottoVariants(): Observable<HttpResponse<LottoVariantDto[]>> {
    return this.httpClient
      .get<LottoVariantDto[]>(Config.getControllerUrl('LottoVariants', ''), {observe: 'response'})
  }

  public updateLottoVariant(lottoVariantModel: LottoVariantModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('LottoVariants', 'UpdateLottoVariants'), lottoVariantModel, {observe: 'response'})
  }

  public deleteLottoVariant(lottoVariantId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('LottoVariants', 'DeleteLottoVariantsById'), lottoVariantId, {observe: 'response'})
  }
}
