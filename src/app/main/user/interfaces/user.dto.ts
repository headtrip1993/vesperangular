export interface UserDto {
  empNumber?: number
  fullName: string
  firstName: string
  lastName: string
  mInitial: string
  userName: string
  password: string
  group: string
  secLevel: string
  stationCode: string
  userCode: string
  bYear: string
  mName: string
  favFood: string
  temp0: boolean
  inActive: string
  transferred: string
  loggedIn: boolean
  loggedDate: Date
  logTime: string
  upt_date: Date
  entryDate: string
}
