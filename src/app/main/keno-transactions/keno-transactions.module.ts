import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { KenoTransactionsService } from './services/keno-transactions.service'
import { KenoTransactionsRoutingModule } from './keno-transactions-routing.module'
import { KenoTransactionsComponent } from './components/keno-transactions.component'
import { SharedModule } from '../../shared/shared.module'


@NgModule({
  declarations: [KenoTransactionsComponent],
  imports: [
    CommonModule,
    KenoTransactionsRoutingModule,
    NgbModalModule,
    FormsModule,
    NgxDatatableModule,
    SharedModule
  ],
  providers: [
    KenoTransactionsService
  ]
})
export class KenoTransactionsModule {
}
