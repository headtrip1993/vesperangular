import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AddCategoryComponent } from './components/add-category/add-category.component'
import { AllCategoriesComponent } from './components/all-categories/all-categories.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddCategoryComponent,
    data: {
      title: 'Category',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllCategoriesComponent,
    data: {
      title: 'Categories',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CategoryRoutingModule {

}
