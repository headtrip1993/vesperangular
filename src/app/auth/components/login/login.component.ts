import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { CookieService } from '../../../core/services/cookie.service'
import { CommonService } from '../../../core/services/common.service'
import { BranchDdlDto } from '../../../core/interfaces/branch-ddl.dto'
import { Subscription } from 'rxjs'
import { AuthService } from '../../services/auth.service'
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm: FormGroup
  public branches: BranchDdlDto[] = []
  public selectedBranch: BranchDdlDto = null
  public isShowLoginForm: boolean

  private subs: Subscription = new Subscription()

  constructor(
    private readonly router: Router,
    private readonly cookieService: CookieService,
    private readonly commonService: CommonService,
    private readonly authService: AuthService,
    private readonly toastrService: ToastrService,
    private readonly activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.isShowLoginForm = false

    this.loginForm = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
    })

    if (this.activatedRoute.snapshot.queryParams.isAdmin) {
      this.selectedBranch = {branchNum: 0, branchDesc: 'admin'}
      this.isShowLoginForm = true
    } else {
      this.selectedBranch = JSON.parse(this.cookieService.getCookie('branch')) ?? null
    }

    this.getBranches()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get isBranchSelected(): boolean {
    if (!!JSON.parse(this.cookieService.getCookie('branch'))) {
      return true
    } else {
      return this.isShowLoginForm
    }
  }

  public saveSelectedBranch(): void {
    this.isShowLoginForm = true
  }

  // public onBranchChange(branch: BranchDdlDto): void {
  //   console.log(branch)
  //   this.selectedBranch = branch
  // }

  public submitForm(): void {
    if (this.loginForm.invalid) {
      return
    }

    this.login(this.loginForm.value.username, this.loginForm.value.password, this.selectedBranch.branchNum.toString())

    // this.cookieService.setCookie('user', JSON.stringify(this.loginForm.value), 356)
    //
    // if (this.loginForm.value.username.toString().toLowerCase().includes('admin')) {
    //   this.router.navigate(['/category/all'])
    // } else {
    //   this.router.navigate(['/lotto-transactions'])
    // }
  }

  private getBranches(): void {
    this.subs.add(
      this.commonService.getBranches()
        .subscribe(response => {
            this.branches = response
            console.log(this.branches)
          }, error => {
            alert(error)
          },
          () => {

          }
        )
    )
  }

  private login(userName: string, password: string, branchNum: string): void {
    this.subs.add(
      this.authService.login(userName, password, branchNum)
        .subscribe(response => {
          console.log(response)
          if (response.status === 200) {
            if (response.body.length <= 0) {
              this.toastrService.error('Error logging in!', 'Error')
              return
            }

            this.toastrService.success('Successfully logged in!', 'Success')

            this.cookieService.setCookie('user', JSON.stringify(response?.body ?? null), 356)
            this.cookieService.setCookie('branch', JSON.stringify(this?.selectedBranch ?? null), 365)

            if (response.body[0].groupId === 3 || response.body[0].groupId === 4 || response.body[0].groupId === 5) {
              this.router.navigate(['/category/all'])
            } else {
              this.router.navigate(['/lotto-transactions'])
            }

          } else {
            this.toastrService.error('Error logging in!', 'Error')
          }
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }
}
