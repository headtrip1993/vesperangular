import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { AddUserComponent } from './components/add-user/add-user.component'
import { AllUsersComponent } from './components/all-users/all-users.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddUserComponent,
    data: {
      title: 'Users',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllUsersComponent,
    data: {
      title: 'Users',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule {

}
