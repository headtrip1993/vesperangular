import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import * as dayjs from 'dayjs'
import { UserDto } from '../../interfaces/user.dto'
import { UserHttpService } from '../../services/user-http.service'

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public usersDTO: UserDto[]
  public tempData: UserDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly userHttpService: UserHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllUsers()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.usersDTO = this.tempData.filter(d => {
      return d.fullName.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: UserDto): void {
    debugger
    console.log(row)
    this.router.navigate(['/user/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: UserDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.userHttpService.deleteUser(row.empNumber)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting user!', 'Error')
                return
              }
              this.toastrService.success('User successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'User has been deleted.',
                'success'
              )
              this.getAllUsers()
            }, error => {
              this.toastrService.error('Error deleting user!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllUsers(): void {
    this.isLoading = true
    this.subs.add(
      this.userHttpService.getAllUsers()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.usersDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
