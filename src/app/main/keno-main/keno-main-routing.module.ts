import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { KenoListComponent } from './components/keno-list.component'

const routes: Routes = [
  {
    path: 'list',
    component: KenoListComponent,
    data: {
      title: 'Keno Main List',
      breadcrumb: ''
    },
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class KenoMainRoutingModule {

}
