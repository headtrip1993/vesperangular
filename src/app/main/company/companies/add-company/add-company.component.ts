import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { CompanyHttpService } from '../../services/company-http.service'

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public companyFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly companyHttpService: CompanyHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.companyFormGroup = new FormGroup({
      companyNum: new FormControl(null, []),
      companyName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      logo: new FormControl(null, [])
    })

    if (this.isEditing) {
      this.companyFormGroup.setValue(history.state.data)
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.companyFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.companyHttpService.addCompany(this.companyFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving company!', 'Error')
              return
            }
            this.toastrService.success('Company successfully saved!', 'Success')
            this.companyFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving company!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.companyHttpService.updateCompany(this.companyFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating company!', 'Error')
              return
            }
            this.toastrService.success('Company successfully updated!', 'Success')
            this.companyFormGroup.reset()
            this.router.navigate(['/company/all'])
          }, error => {
            this.toastrService.error('Error updating company!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }
  }
}
