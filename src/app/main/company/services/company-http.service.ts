import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { CompanyModel } from '../models/company.model'
import { CompanyDto } from '../interfaces/company.dto'

@Injectable()
export class CompanyHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addCompany(companyModel: CompanyModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Company', ''), companyModel, {observe: 'response'})
  }

  public getAllCompanies(): Observable<HttpResponse<CompanyDto[]>> {
    return this.httpClient
      .get<CompanyDto[]>(Config.getControllerUrl('Company', ''), {observe: 'response'})
  }

  public updateCompany(companyModel: CompanyModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Company', 'UpdateCompany'), companyModel, {observe: 'response'})
  }

  public deleteCompany(companyId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Company', 'DeleteCompanyById'), companyId, {observe: 'response'})
  }
}
