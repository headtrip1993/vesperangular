import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { Injectable } from '@angular/core'

@Injectable()
export class KenoMainService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public getAllLottoMain(): Observable<any> {
    return this.httpClient
      .get<any>(Config.getControllerUrl('Keno', ''), {observe: 'response'})
  }
}
