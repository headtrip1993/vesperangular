export interface InventoryDto {
  itemNum?: number
  itemID: number
  itemDesc: string
  nOrder: number
  itemCode: string
  gameCode: string
  units: string
  showInSearch: boolean
  showInReport: boolean
  category: string
  source: string
  unitPrice: number
  booksPerCarton: number
  ticketsPerBook: number
  lowQuantity: number
  userCode: string
  series: string
  pridicts: number
  upt_date: Date
  recordStatus: boolean
}
