export class AreaModel {
  areaNum?: number
  areaId: string
  areaName: string
  regionId: string
  showInSearch: boolean
  userCode: string
}
