import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AddBranchComponent } from './components/add-branch/add-branch.component'
import { AllBranchesComponent } from './components/all-branches/all-branches.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddBranchComponent,
    data: {
      title: 'Branch',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllBranchesComponent,
    data: {
      title: 'Branch',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BranchRoutingModule {

}
