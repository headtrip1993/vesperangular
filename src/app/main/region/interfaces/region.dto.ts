export interface RegionDto {
  upt_date: Date
  regionNum?: number
  regionId: string
  regionName: string
  showInSearch: boolean
  userCode: string
  islandGrouping: string
  islandOrder: number
}
