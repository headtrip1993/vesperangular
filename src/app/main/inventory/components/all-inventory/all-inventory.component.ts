import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import * as dayjs from 'dayjs'
import { InventoryDto } from '../../interfaces/inventory.dto'
import { InventoryHttpService } from '../../services/inventory-http.service'

@Component({
  selector: 'app-all-inventory',
  templateUrl: './all-inventory.component.html',
  styleUrls: ['./all-inventory.component.scss']
})
export class AllInventoryComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public inventoriesDTO: InventoryDto[]
  public tempData: InventoryDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly inventoryHttpService: InventoryHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllInventories()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.inventoriesDTO = this.tempData.filter(d => {
      return d.gameCode.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: InventoryDto): void {
    debugger
    console.log(row)
    this.router.navigate(['/inventory/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: InventoryDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.inventoryHttpService.deleteInventory(row.itemNum)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting inventory!', 'Error')
                return
              }
              this.toastrService.success('Inventory successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Inventory has been deleted.',
                'success'
              )
              this.getAllInventories()
            }, error => {
              this.toastrService.error('Error deleting inventory!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllInventories(): void {
    this.isLoading = true
    this.subs.add(
      this.inventoryHttpService.getAllInventories()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.inventoriesDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
