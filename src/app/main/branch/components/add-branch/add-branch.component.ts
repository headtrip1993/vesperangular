import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { BranchHttpService } from '../../services/branch-http.service'

@Component({
  selector: 'app-add-branch',
  templateUrl: './add-branch.component.html',
  styleUrls: ['./add-branch.component.scss']
})
export class AddBranchComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public branchFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly branchHttpService: BranchHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.branchFormGroup = new FormGroup({
      branchNum: new FormControl(null, []),
      branchId: new FormControl(null, [Validators.required, Validators.maxLength(5)]),
      sgBranch: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      inst_comp: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      lot_comp: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      keno_comp: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      branchDesc: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      areaId: new FormControl(null, [Validators.required, Validators.maxLength(5)]),
      regionId: new FormControl(null, [Validators.required, Validators.maxLength(3)]),
      divisionId: new FormControl(null, [Validators.required, Validators.maxLength(3)]),
      showInSearch: new FormControl(false, []),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      cluster: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      nGroup: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      malls: new FormControl(false, []),
      nLotto: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      mallGrouping: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      agencyNo: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      agencynoKeno: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      agencynoCoronis: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      agencynoExtrema: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      agencyMicrolot: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      isasSortOrder: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      isasSortOrder2: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      classification: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      shortname: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
    })

    if (this.isEditing) {
      this.branchFormGroup.setValue({
        branchNum: history.state.data.branchNum,
        branchId: history.state.data.branchID,
        sgBranch: history.state.data.sgBranch,
        inst_comp: history.state.data.inst_comp,
        lot_comp: history.state.data.lot_comp,
        keno_comp: history.state.data.keno_comp,
        branchDesc: history.state.data.branchDesc,
        areaId: history.state.data.areaID,
        regionId: history.state.data.regionID,
        divisionId: history.state.data.divisionID,
        showInSearch: history.state.data.showInSearch,
        userCode: history.state.data.userCode,
        cluster: history.state.data.cluster,
        nGroup: history.state.data.nGroup,
        malls: history.state.data.malls,
        nLotto: history.state.data.nLotto,
        mallGrouping: history.state.data.mallGrouping,
        agencyNo: history.state.data.agencyNo,
        agencynoKeno: history.state.data.agencynokeno,
        agencynoCoronis: history.state.data.agencynoCoronis,
        agencynoExtrema: history.state.data.agencynoExtrema,
        agencyMicrolot: history.state.data.agencyMicrolot,
        isasSortOrder: history.state.data.isasSortOrder,
        isasSortOrder2: history.state.data.isasSortOrder2,
        classification: history.state.data.classification,
        shortname: history.state.data.shortname,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.branchFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.branchHttpService.addBranch(this.branchFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving branch!', 'Error')
              return
            }
            this.toastrService.success('Branch successfully saved!', 'Success')
            this.branchFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving branch!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.branchHttpService.addBranch(this.branchFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating branch!', 'Error')
              return
            }
            this.toastrService.success('Branch successfully updated!', 'Success')
            this.branchFormGroup.reset()
            this.router.navigate(['/branch/all'])
          }, error => {
            this.toastrService.error('Error updating branch!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }
  }
}
