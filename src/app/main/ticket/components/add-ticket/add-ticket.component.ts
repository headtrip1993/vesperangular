import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { TicketHttpService } from '../../services/ticket-http.service'

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.scss']
})
export class AddTicketComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public ticketFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly ticketHttpService: TicketHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.ticketFormGroup = new FormGroup({
      ticketsId: new FormControl(null, []),
      detailId: new FormControl(null, [Validators.required, Validators.maxLength(18)]),
      denomination: new FormControl(null, [Validators.required, Validators.maxLength(8)]),
      quantity: new FormControl(null, [Validators.required, Validators.maxLength(5)])
    })

    if (this.isEditing) {
      this.ticketFormGroup.setValue({
        ticketsId: history.state.data.ticketsId,
        detailId: history.state.data.detailId,
        denomination: history.state.data.denomination,
        quantity: history.state.data.quantity,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.ticketFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.ticketHttpService.addTicket(this.ticketFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving ticket!', 'Error')
              return
            }
            this.toastrService.success('Ticket successfully saved!', 'Success')
            this.ticketFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving ticket!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.ticketHttpService.updateTicket(this.ticketFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating ticket!', 'Error')
              return
            }
            this.toastrService.success('Ticket successfully updated!', 'Success')
            this.ticketFormGroup.reset()
            this.router.navigate(['/ticket/all'])
          }, error => {
            this.toastrService.error('Error updating ticket!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }
}
