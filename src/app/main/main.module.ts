import { NgModule } from '@angular/core'
import { CategoryModule } from './category/category.module'
import { MainRoutingModule } from './main-routing.module'
import { BranchModule } from './branch/branch.module'
import { RegionModule } from './region/region.module'
import { CompanyModule } from './company/company.module'
import { TicketModule } from './ticket/ticket.module'
import { UserModule } from './user/user.module'
import { RateModule } from './rate/rate.module'
import { InventoryModule } from './inventory/inventory.module'
import { AreaModule } from './area/area.module'
import { LottoVariantModule } from './lotto-variant/lotto-variant.module'
import { InstantsTransactionsModule } from './instants-transactions/instants-transactions.module'
import { LottoTransactionsModule } from './lotto-transactions/lotto-transactions.module'
import { KenoTransactionsModule } from './keno-transactions/keno-transactions.module'
import { LottoMainModule } from './lotto-main/lotto-main.module'
import { KenoMainModule } from './keno-main/keno-main.module';
import { ScanTicketInventoryModule } from './scan-ticket-inventory/scan-ticket-inventory.module'

@NgModule({
  imports: [
    MainRoutingModule,
    CategoryModule,
    BranchModule,
    RegionModule,
    CompanyModule,
    TicketModule,
    UserModule,
    RateModule,
    InventoryModule,
    AreaModule,
    LottoVariantModule,
    InstantsTransactionsModule,
    LottoTransactionsModule,
    KenoTransactionsModule,
    LottoMainModule,
    KenoMainModule,
    ScanTicketInventoryModule
  ]
})
export class MainModule {

}
