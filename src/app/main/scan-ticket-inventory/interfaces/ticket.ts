export interface Ticket {
  id: number,
  branchNum: number,
  type: string,
  variant: string,
  scanTYPE: string,
  tickets: number,
  bookId: string,
  fromticket: number,
  toticket: number,
  trandate: Date,
  totTickets: number,
  cTIME: number,
  posted: boolean,
  transferedBySGBranch: string
}
