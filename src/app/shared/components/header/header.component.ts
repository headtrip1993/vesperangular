import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { NavService, Menu } from '../../services/nav.service'
import { Router } from '@angular/router'
import { CookieService } from '../../../core/services/cookie.service'

var body = document.getElementsByTagName('body')[0]

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public menuItems: Menu[]
  public items: Menu[]
  public openNav: boolean = false
  public right_sidebar: boolean = false
  public text: string

  @Output() rightSidebarEvent = new EventEmitter<boolean>()

  constructor(
    public navServices: NavService,
    private readonly router: Router,
    private readonly cookieService: CookieService
    ) {
  }

  ngOnInit(): void {
    this.navServices.items.subscribe(menuItems => {
      this.items = menuItems
    })
  }

  collapseSidebar(): void {
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar
  }

  logout(): void {
    this.cookieService.deleteCookie('user')
    this.router.navigate(['/auth/login'])
  }
}
