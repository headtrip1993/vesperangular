import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLottoVariantComponent } from './components/add-lotto-variant/add-lotto-variant.component';
import { AllLottoVariantsComponent } from './components/all-lotto-variants/all-lotto-variants.component';
import { LottoVariantRoutingModule } from './lotto-variant-routing.module'
import { LottoVariantHttpService } from './services/lotto-variant-http.service'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'



@NgModule({
  declarations: [AddLottoVariantComponent, AllLottoVariantsComponent],
  imports: [
    CommonModule,
    LottoVariantRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    LottoVariantHttpService
  ]
})
export class LottoVariantModule { }
