import { Directive, HostListener } from '@angular/core'

declare var require: any
let screenfull = require('screenfull')

@Directive({
  selector: '[appToggleFullscreen]'
})
export class ToggleFullscreenDirective {
  @HostListener('click') onClick(): any {
    console.log('Clicked')
    // if (screenfull.enabled) {
    screenfull.toggle()
    // }
  }
}
