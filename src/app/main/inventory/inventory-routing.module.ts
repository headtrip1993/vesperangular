import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { AllInventoryComponent } from './components/all-inventory/all-inventory.component'
import { AddInventoryComponent } from './components/add-inventory/add-inventory.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddInventoryComponent,
    data: {
      title: 'Inventory',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllInventoryComponent,
    data: {
      title: 'Inventory',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class InventoryRoutingModule {

}
