import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { RateHttpService } from '../../services/rate-http.service'

@Component({
  selector: 'app-add-rate',
  templateUrl: './add-rate.component.html',
  styleUrls: ['./add-rate.component.scss']
})
export class AddRateComponent implements OnInit, OnDestroy {
  public isLoading: boolean
  public rateFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly rateHttpService: RateHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.rateFormGroup = new FormGroup({
      id: new FormControl(null, []),
      rateName: new FormControl(null, [Validators.required, Validators.maxLength(35)]),
      rateCode: new FormControl(null, [Validators.required, Validators.maxLength(5)]),
      rateValue: new FormControl(0, [Validators.required, Validators.maxLength(10)]),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
    })

    if (this.isEditing) {
      this.rateFormGroup.setValue({
        id: history.state.data.id,
        rateName: history.state.data.rateName,
        rateCode: history.state.data.rateCode,
        rateValue: history.state.data.rateValue,
        userCode: history.state.data.userCode,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.rateFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.rateHttpService.addRate(this.rateFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving rate!', 'Error')
              return
            }
            this.toastrService.success('Rates successfully saved!', 'Success')
            this.rateFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving rate!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.rateHttpService.updateRate(this.rateFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating rate!', 'Error')
              return
            }
            this.toastrService.success('Rates successfully updated!', 'Success')
            this.rateFormGroup.reset()
            this.router.navigate(['/rate/all'])
          }, error => {
            this.toastrService.error('Error updating rate!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }
}
