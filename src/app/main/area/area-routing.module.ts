import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { AddAreaComponent } from './components/add-area/add-area.component'
import { AllAreasComponent } from './components/all-areas/all-areas.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddAreaComponent,
    data: {
      title: 'Area',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllAreasComponent,
    data: {
      title: 'Areas',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AreaRoutingModule {

}
