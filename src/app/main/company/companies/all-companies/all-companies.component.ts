import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import * as dayjs from 'dayjs'
import { CompanyDto } from '../../interfaces/company.dto'
import { CompanyHttpService } from '../../services/company-http.service'

@Component({
  selector: 'app-all-companies',
  templateUrl: './all-companies.component.html',
  styleUrls: ['./all-companies.component.scss']
})
export class AllCompaniesComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public companiesDTO: CompanyDto[]
  public tempData: CompanyDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly companyHttpService: CompanyHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllCompanies()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.companiesDTO = this.tempData.filter(d => {
      return d.companyName.toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: CompanyDto): void {
    console.log(row)
    this.router.navigate(['/company/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: CompanyDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.companyHttpService.deleteCompany(row.companyNum)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting company!', 'Error')
                return
              }
              this.toastrService.success('Company successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Company has been deleted.',
                'success'
              )
              this.getAllCompanies()
            }, error => {
              this.toastrService.error('Error deleting company!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllCompanies(): void {
    this.isLoading = true
    this.subs.add(
      this.companyHttpService.getAllCompanies()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.companiesDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
