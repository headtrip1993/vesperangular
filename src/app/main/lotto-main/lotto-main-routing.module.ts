import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { LottoListComponent } from './components/lotto-list.component'

const routes: Routes = [
  {
    path: 'list',
    component: LottoListComponent,
    data: {
      title: 'Lotto Main List',
      breadcrumb: ''
    },
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LottoMainRoutingModule {
}
