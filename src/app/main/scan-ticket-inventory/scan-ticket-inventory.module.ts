import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ScanTicketInventoryRoutingModule } from './scan-ticket-inventory-routing.module';
import { ScanTicketInventoryComponent } from './components/scan-ticket-inventory/scan-ticket-inventory.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { StiReceiveTicketsComponent } from './components/sti-receive-tickets/sti-receive-tickets.component';
import { StiTransferTicketsComponent } from './components/sti-transfer-tickets/sti-transfer-tickets.component';


@NgModule({
  declarations: [ScanTicketInventoryComponent, StiReceiveTicketsComponent, StiTransferTicketsComponent],
  imports: [
    CommonModule,
    ScanTicketInventoryRoutingModule,
    NgxDatatableModule,
    NgbNavModule,
    ReactiveFormsModule,
    FormsModule,
    NgbDatepickerModule
  ]
})
export class ScanTicketInventoryModule { }
