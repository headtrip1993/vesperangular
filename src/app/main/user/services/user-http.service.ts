import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { UserModel } from '../models/user.model'
import { UserDto } from '../interfaces/user.dto'

@Injectable()
export class UserHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addUser(userModel: UserModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('User', ''), userModel, {observe: 'response'})
  }

  public getAllUsers(): Observable<HttpResponse<UserDto[]>> {
    return this.httpClient
      .get<UserDto[]>(Config.getControllerUrl('User', ''), {observe: 'response'})
  }

  public updateUser(userModel: UserModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('User', 'UpdateUser'), userModel, {observe: 'response'})
  }

  public deleteUser(userId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('User', 'DeleteUserById'), userId, {observe: 'response'})
  }
}
