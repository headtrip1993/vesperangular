export interface AreaDto {
  areaNum?: number
  areaId: string
  areaName: string
  regionId: string
  showInSearch: boolean
  userCode: string
  upt_date: Date
}
