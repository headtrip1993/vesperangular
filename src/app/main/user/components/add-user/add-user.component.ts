import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { UserHttpService } from '../../services/user-http.service'
import { BranchDdlDto } from '../../../../core/interfaces/branch-ddl.dto'
import { UserGroupDdlDto } from '../../../../core/interfaces/user-group-ddl.dto'
import { CommonService } from '../../../../core/services/common.service'

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public userFormGroup: FormGroup
  public branches: BranchDdlDto[]
  public userGroups: UserGroupDdlDto[]
  public isDisableBranch = false

  private subs: Subscription = new Subscription()

  constructor(
    private readonly userHttpService: UserHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router,
    private readonly commonService: CommonService
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.isLoading = false

    this.userFormGroup = new FormGroup({
      empNumber: new FormControl(null, []),
      fullName: new FormControl(null, [Validators.required, Validators.maxLength(25)]),
      firstName: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      lastName: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      mInitial: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      username: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      password: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      group: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      secLevel: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      stationCode: new FormControl(null, [Validators.required, Validators.maxLength(3)]),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      bYear: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      mName: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      favFood: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      temp0: new FormControl(false, [Validators.required]),
      inActive: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      transferred: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      branch: new FormControl(null, [Validators.required]),
      userGroup: new FormControl(null, [Validators.required])
    })

    if (this.isEditing) {
      this.userFormGroup.setValue({
        empNumber: history.state.data.empNumber,
        fullName: history.state.data.fullName,
        firstName: history.state.data.firstName,
        lastName: history.state.data.lastName,
        mInitial: history.state.data.mInitial,
        username: history.state.data.userName,
        password: history.state.data.password,
        group: history.state.data.group,
        secLevel: history.state.data.secLevel,
        stationCode: history.state.data.stationCode,
        userCode: history.state.data.userCode,
        bYear: history.state.data.bYear,
        mName: history.state.data.mName,
        favFood: history.state.data.favFood,
        temp0: history.state.data.temp0,
        inActive: history.state.data.inActive,
        transferred: history.state.data.transferred,
      })
    }

    await this.getBranchesAndUserGroups()

    this.userFormGroup.controls.branch.disable()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public onUserGroupChange(val): void {
    console.log(val)
    if (val.id === 1 || val.id === 2) {
      this.userFormGroup.controls.branch.enable()
    } else {
      this.userFormGroup.controls.branch.disable()
    }
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.userFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    console.log(this.findInvalidControlsRecursive(this.userFormGroup))
    // return
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.userHttpService.addUser({
          ...this.userFormGroup.value,
          branchNum: this.userFormGroup?.value?.branch?.branchNum ?? 0,
          groupId: this.userFormGroup?.value?.userGroup?.id ?? 0
        })
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving user!', 'Error')
              return
            }
            this.toastrService.success('User successfully saved!', 'Success')
            this.userFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving user!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.userHttpService.updateUser(this.userFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating user!', 'Error')
              return
            }
            this.toastrService.success('User successfully updated!', 'Success')
            this.userFormGroup.reset()
            this.router.navigate(['/user/all'])
          }, error => {
            this.toastrService.error('Error updating user!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }

  private findInvalidControlsRecursive(formToInvestigate: FormGroup | FormArray): string[] {
    const invalidControls: string[] = []
    const recursiveFunc = (form: FormGroup | FormArray) => {
      Object.keys(form.controls).forEach(field => {
        const control = form.get(field)
        if (control.invalid) {
          invalidControls.push(field)
        }
        if (control instanceof FormGroup) {
          recursiveFunc(control)
        } else if (control instanceof FormArray) {
          recursiveFunc(control)
        }
      })
    }
    recursiveFunc(formToInvestigate)
    return invalidControls
  }

  private async getBranchesAndUserGroups(): Promise<void> {
    this.isLoading = true

    const results = await Promise.all([this.commonService.getBranches().toPromise(), this.commonService.getUserGroups().toPromise()])

    this.isLoading = false

    this.branches = results[0]
    this.userGroups = results[1]
  }
}
