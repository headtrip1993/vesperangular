export interface RateDto {
  id?: number
  rateName: string
  rateCode: string
  rateValue: number
  userCode: string
  upt_date: Date
  recordStatus: boolean
}
