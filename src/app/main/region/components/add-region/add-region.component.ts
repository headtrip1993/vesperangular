import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { RegionHttpService } from '../../services/region-http.service'

@Component({
  selector: 'app-add-region',
  templateUrl: './add-region.component.html',
  styleUrls: ['./add-region.component.scss']
})
export class AddRegionComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public regionFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly regionHttpService: RegionHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.regionFormGroup = new FormGroup({
      regionNum: new FormControl(null, []),
      regionId: new FormControl(null, [Validators.required, Validators.maxLength(3)]),
      regionName: new FormControl(null, [Validators.required, Validators.maxLength(25)]),
      showInSearch: new FormControl(false, []),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      islandGrouping: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      islandOrder: new FormControl(null, [Validators.required]),
    })

    if (this.isEditing) {
      this.regionFormGroup.setValue({
        regionNum: history.state.data.regionNum,
        regionId: history.state.data.regionId,
        regionName: history.state.data.regionName,
        showInSearch: history.state.data.showInSearch,
        userCode: history.state.data.userCode,
        islandGrouping: history.state.data.islandGrouping,
        islandOrder: history.state.data.islandOrder,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.regionFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.regionHttpService.addRegion(this.regionFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving region!', 'Error')
              return
            }
            this.toastrService.success('Region successfully saved!', 'Success')
            this.regionFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving region!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.regionHttpService.updateRegion(this.regionFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating region!', 'Error')
              return
            }
            this.toastrService.success('Region successfully updated!', 'Success')
            this.regionFormGroup.reset()
            this.router.navigate(['/region/all'])
          }, error => {
            this.toastrService.error('Error updating region!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }

}
