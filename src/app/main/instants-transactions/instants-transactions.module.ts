import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstantsTransactionsComponent } from './components/instants-transactions/instants-transactions.component';
import { InstantsTransactionsRoutingModule } from './instants-transactions-routing.module'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { InstantsTransactionsService } from './services/instants-transactions.service'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'


@NgModule({
  declarations: [InstantsTransactionsComponent],
  imports: [
    CommonModule,
    InstantsTransactionsRoutingModule,
    NgbModalModule,
    FormsModule,
    NgxDatatableModule
  ],
  providers: [
    InstantsTransactionsService
  ]
})
export class InstantsTransactionsModule { }
