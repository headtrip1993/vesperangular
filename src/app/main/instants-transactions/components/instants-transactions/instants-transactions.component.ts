import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { CookieService } from '../../../../core/services/cookie.service'
import { InstantsTransactionsService } from '../../services/instants-transactions.service'
import { Subscription } from 'rxjs'
import { InventoryItemsDto } from '../../interfaces/inventory-items.dto'
import { DatatableComponent, SelectionType, TableColumn } from '@swimlane/ngx-datatable'
import { BranchDdlDto } from '../../../../core/interfaces/branch-ddl.dto'

@Component({
  selector: 'app-instants-transactions',
  templateUrl: './instants-transactions.component.html',
  styleUrls: ['./instants-transactions.component.scss']
})
export class InstantsTransactionsComponent implements OnInit, OnDestroy {

  @ViewChild('modal')
  public modal: TemplateRef<any>
  @ViewChild('datatableComponent')
  public dataTableComponent: DatatableComponent
  @ViewChild('datatableComponentMain')
  public dataTableComponentMain: DatatableComponent

  public isLoading: boolean
  public branchName: string
  public inventoryItems: InventoryItemsDto[] = null
  public selectedInventoryItem: InventoryItemsDto = null
  public columns: TableColumn[] = []
  public mainColumn: TableColumn[] = []

  public details: any[] = []
  public mainRows: any[] = []
  public selectedRows: any[] = []
  public selectionType: SelectionType = SelectionType.checkbox
  public isSelectAllRowsOnPage = false
  public payouts: number
  public stampTax: number

  private subs: Subscription = new Subscription()

  public detailModel = {
    itemCode: null,
    sales: null,
    commission: null,
    cancels: null,
  }

  constructor(
    public readonly ngbModal: NgbModal,
    private readonly cookieService: CookieService,
    private readonly instantsTransactionsService: InstantsTransactionsService
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.branchName = JSON.parse(this.cookieService.getCookie('branch')).branchDesc
    this.getInventoryItemsDropdown()

    this.mainColumn = [
      {
        name: 'Sales',
        prop: 'sales'
      },
      {
        name: 'DST',
        prop: 'dst'
      },
      {
        name: 'Cancels',
        prop: 'cancels'
      },
      {
        name: 'Payouts',
        prop: 'payouts'
      },
      {
        name: 'Net Sales',
        prop: 'netSales'
      },
      {
        name: 'Cash Sales',
        prop: 'cashSales'
      },
      {
        name: 'Commission',
        prop: 'commission'
      },
      {
        name: 'WH Tax',
        prop: 'whTax'
      },
      {
        name: 'Net Comm',
        prop: 'netComm'
      },
      {
        name: 'Balance',
        prop: 'balance'
      },
      {
        name: 'Adjustment',
        prop: 'adjustment'
      },
      {
        name: 'Net Due',
        prop: 'netDue'
      }
    ]

    this.columns = [
      {
        width: 30,
        sortable: false,
        canAutoResize: false,
        draggable: false,
        resizeable: false,
        headerCheckboxable: true,
        checkboxable: true
      },
      {
        name: 'Item Code',
        prop: 'itemCode'
      },
      {
        name: 'Sales',
        prop: 'sales'
      },
      {
        name: 'Commission',
        prop: 'commission'
      },
      {
        name: 'Cancels',
        prop: 'cancels'
      }
    ]
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get rows(): any[] {
    return this.details.map(a => {
      return {
        ...a,
        itemCode: a.itemCode.itemCode
      }
    })
  }

  public get mainTableRows(): any[] {
    return this.mainRows.map(a => {
      return {...a}
    })
  }

  public onAdd(): void {
    this.ngbModal.open(this.modal, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    })
  }

  public onMainSave(): void {
    debugger
    this.isLoading = true
    const branch: BranchDdlDto = JSON.parse(this.cookieService.getCookie('branch'))

    const temp = {
      transcationid: 0,
      transxn_id: 'string',
      branchId: branch.branchNum.toString(),
      branchDesc: branch.branchDesc,
      tranDate: '2021-03-03T15:58:51.163Z',
      tranTime: 'string',
      beginningCash: 0,
      cashSales: this.mainRows[0].cashSales,
      cashDeposit: 0,
      otherCashDep: 0,
      endingCash: 0,
      overAge: 0,
      upt_Date: '2021-03-03T15:58:51.163Z',
      userCode: JSON.parse(this.cookieService.getCookie('user')).username,
      forPosting: true,
      posted: true,
      postedUser: 'string',
      postedDate: '2021-03-03T15:58:51.163Z',
      accountId: 'string',
      deptId: 'string',
      subDeptId: 'string',
      category: 'string',
      cashRemaining: 0,
      cashPartial: 0,
      deptTime: '2021-03-03T15:58:51.163Z',
      remarks: 'string',
      renTime: 'string',
      pcsoTime: 'string',
      modifiedBy: 'string',
      modifiedDate: '2021-03-03T15:58:51.163Z',
      ds: 0,
      lottoDtls: this.rows.map(a => {
        return {
          detailId: 0,
          transcationId: 0,
          transxn_Id: 'string',
          sales: a.sales,
          cancels: a.cancels,
          payout: 0,
          netSales: 0,
          cashSales: 0,
          commission: a.commission,
          whTax: 0,
          netCommission: 0,
          balance: 0,
          overage: 0,
          netDue: 0,
          netDueValidate: 0,
          begBal: 0,
          dst: 0
        }
      })
    }

    const mainObj = {
      branchId: branch.branchNum.toString(),
      branchDesc: branch.branchDesc,
      beginningCash: 0,
      cashSales: this.mainRows[0].cashSales,
      cashDeposit: 0,
      otherCashDep: 0,
      endingCash: 0,
      userCode: JSON.parse(this.cookieService.getCookie('user')).username,
      lottoDtls: this.rows
    }

    this.subs.add(
      this.instantsTransactionsService.saveInstantTransaction(mainObj)
        .subscribe(response => {
          console.log(response)
          this.isLoading = false
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }

  public onSaveModal(): void {
    this.mainRows.push(
      {
        sales: this.sumCancelsSales()[1],
        dst: 0,
        cancels: this.sumCancelsSales()[0],
        payouts: this.payouts ?? 0,
        netSales: 0,
        cashSales: this.sumCancelsSales()[1],
        commission: (this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) * this.stampTax ?? 0,
        whTax: 0,
        netComm: ((this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) * this.stampTax ?? 0) - this.stampTax ?? 0,
        balance: this.sumCancelsSales()[1] - this.sumCancelsSales()[0] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) + this.stampTax ?? 0 + 0,
        adjustment: 0,
        netDue: (this.sumCancelsSales()[1] - this.sumCancelsSales()[0] - 0 + this.payouts ?? 0 - (this.sumCancelsSales()[1] - this.sumCancelsSales()[0]) + this.stampTax ?? 0 + 0) + 0
      }
    )

    this.ngbModal.dismissAll()
  }

  public onSelect({selected}): void {
    console.log('Select Event', selected, this.selectedRows)

    this.selectedRows.splice(0, this.selectedRows.length)
    this.selectedRows.push(...selected)
  }

  public onBackModal(): void {
    this.ngbModal.dismissAll()
  }

  public onDetailAdd(): void {
    this.details.push(this.detailModel)
    this.detailModel = {
      itemCode: null,
      sales: null,
      commission: null,
      cancels: null,
    }
  }

  public sumCancelsSales(): number[] {
    let cancelsSum = 0
    let salesSum = 0

    this.details.forEach(d => {
      cancelsSum += d.cancels
    })

    this.details.forEach(d => {
      salesSum += d.sales
    })

    return [cancelsSum, salesSum]
  }

  private getInventoryItemsDropdown(): void {
    this.isLoading = true
    this.subs.add(
      this.instantsTransactionsService
        .getInventoryItemsDropdown()
        .subscribe(response => {
          this.inventoryItems = response
          this.isLoading = false
        }, error => {
          alert(error)
        }, () => {

        })
    )
  }

}
