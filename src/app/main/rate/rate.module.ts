import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddRateComponent } from './components/add-rate/add-rate.component';
import { AllRatesComponent } from './components/all-rates/all-rates.component';
import { RateHttpService } from './services/rate-http.service'
import { RateRoutingModule } from './rate-routing.module'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'



@NgModule({
  declarations: [AddRateComponent, AllRatesComponent],
  imports: [
    CommonModule,
    RateRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    RateHttpService
  ]
})
export class RateModule { }
