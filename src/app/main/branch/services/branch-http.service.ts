import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { BranchModel } from '../models/branch.model'
import { Config } from 'src/app/core/helpers/config.helper'
import { BranchDto } from '../interfaces/branch.dto'

@Injectable()
export class BranchHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addBranch(branchModel: BranchModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Branch', ''), branchModel, {observe: 'response'})
  }

  public getAllBranched(): Observable<HttpResponse<BranchDto[]>> {
    return this.httpClient
      .get<BranchDto[]>(Config.getControllerUrl('Branch', ''), {observe: 'response'})
  }

  public updateBranch(branchModel: BranchModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Branch', 'UpdateBranch'), branchModel, {observe: 'response'})
  }

  public deleteBranch(branchId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Branch', 'DeleteBranchById'), branchId, {observe: 'response'})
  }
}
