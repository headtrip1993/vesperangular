import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTicketComponent } from './components/add-ticket/add-ticket.component';
import { AllTicketsComponent } from './components/all-tickets/all-tickets.component';
import { TicketRoutingModule } from './ticket-routing.module'
import { TicketHttpService } from './services/ticket-http.service'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'



@NgModule({
  declarations: [AddTicketComponent, AllTicketsComponent],
  imports: [
    CommonModule,
    TicketRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    TicketHttpService
  ]
})
export class TicketModule { }
