import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { InventoryModel } from '../models/inventory.model'
import { InventoryDto } from '../interfaces/inventory.dto'

@Injectable()
export class InventoryHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addInventory(inventoryModel: InventoryModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('InvItems', ''), inventoryModel, {observe: 'response'})
  }

  public getAllInventories(): Observable<HttpResponse<InventoryDto[]>> {
    return this.httpClient
      .get<InventoryDto[]>(Config.getControllerUrl('InvItems', ''), {observe: 'response'})
  }

  public updateInventory(inventoryModel: InventoryModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('InvItems', 'UpdateInvItems'), inventoryModel, {observe: 'response'})
  }

  public deleteInventory(inventoryId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('InvItems', 'DeleteInvItemsById'), inventoryId, {observe: 'response'})
  }
}
