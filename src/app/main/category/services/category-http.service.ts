import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'
import { CategoryModel } from '../models/category.model'
import { Observable } from 'rxjs'
import { Config } from '../../../core/helpers/config.helper'
import { CategoryDto } from '../interfaces/category.dto'

@Injectable()
export class CategoryHttpService {
  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  public addCategory(categoryModel: CategoryModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Category', ''), categoryModel, {observe: 'response'})
  }

  public getAllCategories(): Observable<HttpResponse<CategoryDto[]>> {
    return this.httpClient
      .get<CategoryDto[]>(Config.getControllerUrl('Category', ''), {observe: 'response'})
  }

  public updateCategory(categoryModel: CategoryModel): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Category', 'UpdateCategory'), categoryModel, {observe: 'response'})
  }

  public deleteCategory(categoryId: number): Observable<HttpResponse<any>> {
    return this.httpClient
      .post<HttpResponse<any>>(Config.getControllerUrl('Category', 'DeleteCategoryById'), categoryId, {observe: 'response'})
  }
}
