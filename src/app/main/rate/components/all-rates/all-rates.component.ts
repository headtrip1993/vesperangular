import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { DatatableComponent } from '@swimlane/ngx-datatable'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import * as dayjs from 'dayjs'
import { RateDto } from '../../interfaces/rate.dto'
import { RateHttpService } from '../../services/rate-http.service'

@Component({
  selector: 'app-all-rates',
  templateUrl: './all-rates.component.html',
  styleUrls: ['./all-rates.component.scss']
})
export class AllRatesComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public ratesDTO: RateDto[]
  public tempData: RateDto[] = []

  @ViewChild('datatableComponent')
  public dataTable: DatatableComponent

  private subs: Subscription = new Subscription()

  constructor(
    private readonly rateHttpService: RateHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false
    this.getAllRates()
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public updateFilter(event): void {
    const val = event.target.value.toLowerCase()

    // filter our data
    // update the rows
    this.ratesDTO = this.tempData.filter(d => {
      return d.rateName.toString().toLowerCase().indexOf(val) !== -1 || !val
    })
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0
  }

  public onEdit(row: RateDto): void {
    console.log(row)
    this.router.navigate(['/rate/add'], {
      state: {
        data: row
      }
    })
  }

  public onDelete(row: RateDto): void {
    console.log(row)
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.subs.add(
          this.rateHttpService.deleteRate(row.id)
            .subscribe(response => {
              console.log(response)
              if (response.status !== 200) {
                this.toastrService.error('Error deleting rate!', 'Error')
                return
              }
              this.toastrService.success('Rate successfully deleted!', 'Success')
              Swal.fire(
                'Deleted!',
                'Rate has been deleted.',
                'success'
              )
              this.getAllRates()
            }, error => {
              this.toastrService.error('Error deleting rate!', 'Error')
            }, () => {
              this.isLoading = false
            }))
      }
    })
  }

  public formatDate(date: Date): string {
    return dayjs(date).format('D-MMM-YYYY')
  }

  private getAllRates(): void {
    this.isLoading = true
    this.subs.add(
      this.rateHttpService.getAllRates()
        .subscribe(response => {
          if (response.status !== 200) {
            this.toastrService.error('Error occurred!')
          }
          this.ratesDTO = response?.body ?? []
          this.tempData = [...response?.body]
        }, error => {
          this.toastrService.error('Error occurred!')
        }, () => {
          this.isLoading = false
        })
    )
  }
}
