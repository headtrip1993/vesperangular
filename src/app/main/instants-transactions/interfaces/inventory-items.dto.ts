export interface InventoryItemsDto {
  itemNum: number
  itemCode: string
}
