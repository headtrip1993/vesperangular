import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StiCsvParserService {

  constructor() { }

  parseCSV(csvFile: string) {
    let csvRecordsArray = (<string>csvFile).split(/\r\n|\n/);

    let headersRow = this.getHeaderArray(csvRecordsArray);
    let records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);

    return records
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let csvArr = [];
    for (let i = 1; i < csvRecordsArray.length; i++) {
      let currentRecord = (<string>csvRecordsArray[i]).split(',');

      if (currentRecord.length == headerLength) {
        let csvRecord: any = {
          variant: currentRecord[0].trim(),
          trandate: new Date(currentRecord[1].trim()).toISOString(),
          tickets: +currentRecord[2].trim(),
          type: currentRecord[3].trim(),
          scanTYPE: currentRecord[4].trim(),
          bookId: currentRecord[5].trim(),
          fromticket: +currentRecord[6].trim(),
          toticket: +currentRecord[7].trim(),
          totTickets: +currentRecord[8].trim(),
          // csvRecord.transferedBySGBranch = currentRecord[8].trim();
        }

        csvArr.push(csvRecord);
      }
    }

    return csvArr;
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];

    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }

    return headerArray;
  }
}
