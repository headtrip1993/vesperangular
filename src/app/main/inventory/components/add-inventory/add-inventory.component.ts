import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { InventoryHttpService } from '../../services/inventory-http.service'

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.scss']
})
export class AddInventoryComponent implements OnInit, OnDestroy {


  public isLoading: boolean
  public inventoryFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly inventoryHttpService: InventoryHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.inventoryFormGroup = new FormGroup({
      itemNum: new FormControl(null, []),
      itemId: new FormControl(null, [Validators.required, Validators.maxLength(18)]),
      itemDesc: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      nOrder: new FormControl(null, [Validators.required, Validators.maxLength(12)]),
      itemCode: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      gameCode: new FormControl(null, [Validators.required, Validators.maxLength(3)]),
      units: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      showInSearch: new FormControl(false, [Validators.required]),
      showInReport: new FormControl(false, [Validators.required]),
      category: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      source: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      unitPrice: new FormControl(null, [Validators.required, Validators.maxLength(8)]),
      booksPerCarton: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      ticketsPerBook: new FormControl(null, [Validators.required, Validators.maxLength(6)]),
      lowQuantity: new FormControl(null, [Validators.required, Validators.maxLength(8)]),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(25)]),
      series: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      pridicts: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
    })

    if (this.isEditing) {
      this.inventoryFormGroup.setValue({
        itemNum: history.state.data.itemNum,
        itemId: history.state.data.itemID,
        itemDesc: history.state.data.itemDesc,
        nOrder: history.state.data.nOrder,
        itemCode: history.state.data.itemCode,
        gameCode: history.state.data.gameCode,
        units: history.state.data.units,
        showInSearch: history.state.data.showInSearch,
        showInReport: history.state.data.showInReport,
        category: history.state.data.category,
        source: history.state.data.source,
        unitPrice: history.state.data.unitPrice,
        booksPerCarton: history.state.data.booksPerCarton,
        ticketsPerBook: history.state.data.ticketsPerBook,
        lowQuantity: history.state.data.lowQuantity,
        userCode: history.state.data.userCode,
        series: history.state.data.series,
        pridicts: history.state.data.pridicts,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.inventoryFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    console.log(this.findInvalidControlsRecursive(this.inventoryFormGroup))
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.inventoryHttpService.addInventory(this.inventoryFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving inventory!', 'Error')
              return
            }
            this.toastrService.success('Inventory successfully saved!', 'Success')
            this.inventoryFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving inventory!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.inventoryHttpService.updateInventory(this.inventoryFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating inventory!', 'Error')
              return
            }
            this.toastrService.success('Inventory successfully updated!', 'Success')
            this.inventoryFormGroup.reset()
            this.router.navigate(['/inventory/all'])
          }, error => {
            this.toastrService.error('Error updating inventory!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }

  private findInvalidControlsRecursive(formToInvestigate: FormGroup | FormArray): string[] {
    const invalidControls: string[] = []
    const recursiveFunc = (form: FormGroup | FormArray) => {
      Object.keys(form.controls).forEach(field => {
        const control = form.get(field)
        if (control.invalid) {
          invalidControls.push(field)
        }
        if (control instanceof FormGroup) {
          recursiveFunc(control)
        } else if (control instanceof FormArray) {
          recursiveFunc(control)
        }
      })
    }
    recursiveFunc(formToInvestigate)
    return invalidControls
  }
}
