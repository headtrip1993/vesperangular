import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { LottoVariantHttpService } from '../../services/lotto-variant-http.service'

@Component({
  selector: 'app-add-lotto-variant',
  templateUrl: './add-lotto-variant.component.html',
  styleUrls: ['./add-lotto-variant.component.scss']
})
export class AddLottoVariantComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public lottoVariantFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly lottoVariantHttpService: LottoVariantHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.lottoVariantFormGroup = new FormGroup({
      itemNum: new FormControl(null, []),
      itemId: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      itemDesc: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      itemCode: new FormControl(null, [Validators.required, Validators.maxLength(4)]),
      commRate: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      showInSearch: new FormControl(false, [Validators.required]),
      showInReport: new FormControl(false, [Validators.required]),
      unitPrice: new FormControl(null, [Validators.required, Validators.maxLength(8)]),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(25)]),
      lottoGame: new FormControl(false, [Validators.required]),
    })

    if (this.isEditing) {
      this.lottoVariantFormGroup.setValue({
        itemNum: history.state.data.itemNum,
        itemId: history.state.data.itemID,
        itemDesc: history.state.data.itemDesc,
        itemCode: history.state.data.itemCode,
        commRate: history.state.data.commRate,
        showInSearch: history.state.data.showInSearch,
        showInReport: history.state.data.showInReport,
        unitPrice: history.state.data.unitPrice,
        userCode: history.state.data.userCode,
        lottoGame: history.state.data.lottoGame,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.lottoVariantFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    console.log(this.findInvalidControlsRecursive(this.lottoVariantFormGroup))
    // return
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.lottoVariantHttpService.addLottoVariant(this.lottoVariantFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving lotto variant!', 'Error')
              return
            }
            this.toastrService.success('Lotto Variant successfully saved!', 'Success')
            this.lottoVariantFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving lotto variant!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.lottoVariantHttpService.updateLottoVariant(this.lottoVariantFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating lotto variant!', 'Error')
              return
            }
            this.toastrService.success('Lotto Variant successfully updated!', 'Success')
            this.lottoVariantFormGroup.reset()
            this.router.navigate(['/lotto-variant/all'])
          }, error => {
            this.toastrService.error('Error updating lotto variant!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }

  private findInvalidControlsRecursive(formToInvestigate: FormGroup | FormArray): string[] {
    const invalidControls: string[] = []
    const recursiveFunc = (form: FormGroup | FormArray) => {
      Object.keys(form.controls).forEach(field => {
        const control = form.get(field)
        if (control.invalid) {
          invalidControls.push(field)
        }
        if (control instanceof FormGroup) {
          recursiveFunc(control)
        } else if (control instanceof FormArray) {
          recursiveFunc(control)
        }
      })
    }
    recursiveFunc(formToInvestigate)
    return invalidControls
  }
}
