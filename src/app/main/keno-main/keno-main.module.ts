import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { SharedModule } from '../../shared/shared.module'
import { KenoMainRoutingModule } from './keno-main-routing.module'
import { KenoMainService } from './services/keno-main.service'
import { KenoListComponent } from './components/keno-list.component'

@NgModule({
  declarations: [KenoListComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    FormsModule,
    NgxDatatableModule,
    KenoMainRoutingModule,
    SharedModule
  ],
  providers: [
    KenoMainService
  ]
})
export class KenoMainModule {
}
