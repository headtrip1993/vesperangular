import { Component, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { AreaHttpService } from '../../services/area-http.service'

@Component({
  selector: 'app-add-area',
  templateUrl: './add-area.component.html',
  styleUrls: ['./add-area.component.scss']
})
export class AddAreaComponent implements OnInit, OnDestroy {

  public isLoading: boolean
  public areaFormGroup: FormGroup

  private subs: Subscription = new Subscription()

  constructor(
    private readonly areaHttpService: AreaHttpService,
    private readonly toastrService: ToastrService,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false

    this.areaFormGroup = new FormGroup({
      areaNum: new FormControl(null, []),
      areaId: new FormControl(null, [Validators.required, Validators.maxLength(5)]),
      areaName: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      regionId: new FormControl(null, [Validators.required, Validators.maxLength(3)]),
      showInSearch: new FormControl(false, [Validators.required]),
      userCode: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
    })

    if (this.isEditing) {
      this.areaFormGroup.setValue({
        areaNum: history.state.data.areaNum,
        areaId: history.state.data.areaId,
        areaName: history.state.data.areaName,
        regionId: history.state.data.regionId,
        showInSearch: history.state.data.showInSearch,
        userCode: history.state.data.userCode,
      })
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }

  public get form(): {
    [key: string]: AbstractControl;
  } {
    return this.areaFormGroup.controls
  }

  public get isEditing(): boolean {
    return !!history.state.data
  }

  public submitForm(): void {
    console.log(this.findInvalidControlsRecursive(this.areaFormGroup))
    // return
    this.isLoading = true

    if (!this.isEditing) {
      this.subs.add(
        this.areaHttpService.addArea(this.areaFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error saving area!', 'Error')
              return
            }
            this.toastrService.success('Area successfully saved!', 'Success')
            this.areaFormGroup.reset()
          }, error => {
            this.toastrService.error('Error saving area!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    } else {
      this.subs.add(
        this.areaHttpService.updateArea(this.areaFormGroup.value)
          .subscribe(response => {
            console.log(response)
            if (response.status !== 200) {
              this.toastrService.error('Error updating area!', 'Error')
              return
            }
            this.toastrService.success('Area successfully updated!', 'Success')
            this.areaFormGroup.reset()
            this.router.navigate(['/area/all'])
          }, error => {
            this.toastrService.error('Error updating area!', 'Error')
          }, () => {
            this.isLoading = false
          }))
    }

  }

  private findInvalidControlsRecursive(formToInvestigate: FormGroup | FormArray): string[] {
    const invalidControls: string[] = []
    const recursiveFunc = (form: FormGroup | FormArray) => {
      Object.keys(form.controls).forEach(field => {
        const control = form.get(field)
        if (control.invalid) {
          invalidControls.push(field)
        }
        if (control instanceof FormGroup) {
          recursiveFunc(control)
        } else if (control instanceof FormArray) {
          recursiveFunc(control)
        }
      })
    }
    recursiveFunc(formToInvestigate)
    return invalidControls
  }
}
