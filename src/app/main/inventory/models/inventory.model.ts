export class InventoryModel {
  itemNum?: number
  itemId: number
  itemDesc: string
  nOrder: number
  itemCode: string
  gameCode: string
  units: string
  showInSearch: boolean
  showInReport: boolean
  category: string
  source: string
  unitPrice: number
  booksPerCarton: number
  ticketsPerBook: number
  lowQuantity: number
  userCode: string
  series: string
  pridicts: number
}
