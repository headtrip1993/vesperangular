import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddUserComponent } from './components/add-user/add-user.component';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { UserRoutingModule } from './user-routing.module'
import { UserHttpService } from './services/user-http.service'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedModule } from '../../shared/shared.module'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'



@NgModule({
  declarations: [AddUserComponent, AllUsersComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    NgxDatatableModule
  ],
  providers: [
    UserHttpService
  ]
})
export class UserModule { }
