import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAreaComponent } from './components/add-area/add-area.component';
import { AllAreasComponent } from './components/all-areas/all-areas.component';
import { AreaRoutingModule } from './area-routing.module'
import { AreaHttpService } from './services/area-http.service'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedModule } from '../../shared/shared.module'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'



@NgModule({
  declarations: [AddAreaComponent, AllAreasComponent],
  imports: [
    CommonModule,
    AreaRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    NgxDatatableModule
  ],
  providers: [
    AreaHttpService
  ]
})
export class AreaModule { }
