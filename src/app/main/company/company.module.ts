import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCompanyComponent } from './companies/add-company/add-company.component';
import { AllCompaniesComponent } from './companies/all-companies/all-companies.component';
import { CompanyRoutingModule } from './company-routing.module'
import { CompanyHttpService } from './services/company-http.service'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'



@NgModule({
  declarations: [AddCompanyComponent, AllCompaniesComponent],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule,
  ],
  providers: [
    CompanyHttpService
  ]
})
export class CompanyModule { }
