import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { AddLottoVariantComponent } from './components/add-lotto-variant/add-lotto-variant.component'
import { AllLottoVariantsComponent } from './components/all-lotto-variants/all-lotto-variants.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddLottoVariantComponent,
    data: {
      title: 'Lotto Variant',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllLottoVariantsComponent,
    data: {
      title: 'Lotto Variants',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LottoVariantRoutingModule {

}
