import { Injectable, HostListener } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { CookieService } from '../../core/services/cookie.service'

// Menu
export interface Menu {
  path?: string,
  title?: string,
  icon?: string,
  type?: string,
  badgeType?: string,
  badgeValue?: string,
  active?: boolean,
  bookmark?: boolean,
  children?: Menu[],
}

@Injectable({
  providedIn: 'root'
})

export class NavService {

  constructor(
    private readonly cookieService: CookieService
  ) {
    this.onResize()
    if (this.screenWidth < 991) {
      this.collapseSidebar = true
    }
  }

  public screenWidth: any
  public collapseSidebar = false

  public get isUserAdmin(): boolean {
    const user = JSON.parse(this.cookieService.getCookie('user'))
    return user[0].groupId === 3 || user[0].groupId === 4 || user[0].groupId === 5
  }

  MENU_ITEMS: Menu[] = []

  // MENU_ITEMS: Menu[] = !this.isUserAdmin ?
  //   [
  //     {
  //       title: 'Instants Transactions',
  //       icon: 'award',
  //       type: 'link',
  //       path: '/instants-transactions'
  //     }
  //   ]
  //   :
  //   [
  //     {
  //       title: 'Categories',
  //       icon: 'layers',
  //       type: 'sub',
  //       children: [
  //         {path: '/category/add', title: 'Add Category', type: 'link'},
  //         {path: '/category/all', title: 'All Categories', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Branches',
  //       icon: 'git-branch',
  //       type: 'sub',
  //       children: [
  //         {path: '/branch/add', title: 'Add Branch', type: 'link'},
  //         {path: '/branch/all', title: 'All Branches', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Regions',
  //       icon: 'credit-card',
  //       type: 'sub',
  //       children: [
  //         {path: '/region/add', title: 'Add Region', type: 'link'},
  //         {path: '/region/all', title: 'All Regions', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Companies',
  //       icon: 'home',
  //       type: 'sub',
  //       children: [
  //         {path: '/company/add', title: 'Add Company', type: 'link'},
  //         {path: '/company/all', title: 'All Companies', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Tickets',
  //       icon: 'award',
  //       type: 'sub',
  //       children: [
  //         {path: '/ticket/add', title: 'Add Ticket', type: 'link'},
  //         {path: '/ticket/all', title: 'All Tickets', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Users',
  //       icon: 'user',
  //       type: 'sub',
  //       children: [
  //         {path: '/user/add', title: 'Add User', type: 'link'},
  //         {path: '/user/all', title: 'All Users', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Rates',
  //       icon: 'dollar-sign',
  //       type: 'sub',
  //       children: [
  //         {path: '/rate/add', title: 'Add Rate', type: 'link'},
  //         {path: '/rate/all', title: 'All Rates', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Inventory',
  //       icon: 'tool',
  //       type: 'sub',
  //       children: [
  //         {path: '/inventory/add', title: 'Add Inventory', type: 'link'},
  //         {path: '/inventory/all', title: 'All Inventory', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Area',
  //       icon: 'award',
  //       type: 'sub',
  //       children: [
  //         {path: '/area/add', title: 'Add Area', type: 'link'},
  //         {path: '/area/all', title: 'All Areas', type: 'link'},
  //       ]
  //     },
  //     {
  //       title: 'Lotto Variants',
  //       icon: 'book',
  //       type: 'sub',
  //       children: [
  //         {path: '/lotto-variant/add', title: 'Add Lotto Variant', type: 'link'},
  //         {path: '/lotto-variant/all', title: 'All Lotto Variants', type: 'link'},
  //       ]
  //     },
  //     // {
  //     //   title: 'Starter Kit', icon: 'anchor', type: 'sub', badgeType: 'primary', active: true, children:
  //     //     [
  //     //       {
  //     //         title: 'Color Version', type: 'sub', active: true, children: [
  //     //           {path: '/color-version/light', title: 'Layout Light', type: 'link'},
  //     //           {path: '/color-version/dark', title: 'Layout Dark', type: 'link'}
  //     //         ]
  //     //       },
  //     //       {
  //     //         title: 'Sidebar', type: 'sub', active: false, children: [
  //     //           {path: '/sidebar/light-sidebar', title: 'Light Sidebar', type: 'link'},
  //     //           {path: '/sidebar/compact-sidebar', title: 'Compact Sidebar', type: 'link', bookmark: true},
  //     //           {path: '/sidebar/icon-sidebar', title: 'Compact Icon Sidebar', type: 'link'},
  //     //           {path: '/sidebar/dark-sidebar', title: 'Dark Sidebar', type: 'link'},
  //     //           {path: '/sidebar/hidden-sidebar', title: 'Sidebar Hidden', type: 'link', bookmark: true},
  //     //           {path: '/sidebar/fixed-sidebar', title: 'Sidebar Fixed', type: 'link'},
  //     //           {path: '/sidebar/image-sidebar', title: 'Sidebar With Image', type: 'link'},
  //     //           {path: '/sidebar/disable', title: 'Disable', type: 'link', bookmark: true},
  //     //         ]
  //     //       },
  //     //       {
  //     //         title: 'Page Layout', type: 'sub', active: false, children: [
  //     //           {path: '/page-layout/boxed', title: 'Boxed', type: 'link', bookmark: true},
  //     //           {path: '/page-layout/rtl', title: 'RTL', type: 'link', bookmark: true},
  //     //           {path: '/page-layout/column', title: '1 Column', type: 'link'},
  //     //         ]
  //     //       },
  //     //       {
  //     //         title: 'Menu Options', type: 'sub', active: false, children: [
  //     //           {path: '/menu-option/scroll', title: 'Hide menu on Scroll', type: 'link'},
  //     //           {path: '/menu-option/vertical', title: 'Vertical Menu', type: 'link'},
  //     //           {path: '/menu-option/mega', title: 'Mega Menu', type: 'link', bookmark: true},
  //     //           {path: '/menu-option/fix-header', title: 'Fix Header', type: 'link'},
  //     //           {path: '/menu-option/header', title: 'Fix Header & Sidebar', type: 'link'},
  //     //         ]
  //     //       },
  //     //       {
  //     //         title: 'Footers', type: 'sub', active: false, children: [
  //     //           {path: '/footer/light-footer', title: 'Footer Light', type: 'link', bookmark: true},
  //     //           {path: '/footer/dark-footer', title: 'Footer Dark', type: 'link'},
  //     //           {path: '/footer/fixed-footer', title: 'Footer Fixed', type: 'link'},
  //     //         ]
  //     //       }
  //     //     ]
  //     // },
  //     // {
  //     //   path: '/raise-support', title: 'Raise Support', icon: 'headphones', type: 'link'
  //     // },
  //     // {
  //     //   path: '/documentation', title: 'Documentation', icon: 'file-text', type: 'link'
  //     // }
  //   ]
  items = new BehaviorSubject<Menu[]>(this.MENU_ITEMS)

  // Windows width
  @HostListener('window:resize', ['$event'])
  onResize(event?: any): any {
    this.screenWidth = window.innerWidth
  }


}
