import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RegionRoutingModule } from './region-routing.module'
import { AddRegionComponent } from './components/add-region/add-region.component'
import { AllRegionsComponent } from './components/all-regions/all-regions.component'
import { RegionHttpService } from './services/region-http.service'
import { SharedModule } from '../../shared/shared.module'
import { ReactiveFormsModule } from '@angular/forms'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'


@NgModule({
  declarations: [AddRegionComponent, AllRegionsComponent],
  imports: [
    CommonModule,
    RegionRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    RegionHttpService
  ]
})
export class RegionModule {
}
