import { RouterModule, Routes } from '@angular/router'
import { InstantsTransactionsComponent } from './components/instants-transactions/instants-transactions.component'
import { NgModule } from '@angular/core'

const routes: Routes = [
  {
    path: '',
    component: InstantsTransactionsComponent,
    data: {
      title: 'Instants Transactions',
      breadcrumb: ''
    },
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class InstantsTransactionsRoutingModule {

}
