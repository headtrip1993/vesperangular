import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'
import { KenoTransactionsComponent } from './components/keno-transactions.component'

const routes: Routes = [
  {
    path: 'transactions',
    component: KenoTransactionsComponent,
    data: {
      title: 'Keno Transactions',
      breadcrumb: ''
    },
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class KenoTransactionsRoutingModule {

}
