import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AddRegionComponent } from './components/add-region/add-region.component'
import { AllRegionsComponent } from './components/all-regions/all-regions.component'

const routes: Routes = [
  {
    path: 'add',
    component: AddRegionComponent,
    data: {
      title: 'Region',
      breadcrumb: 'add'
    },
  },
  {
    path: 'all',
    component: AllRegionsComponent,
    data: {
      title: 'Regions',
      breadcrumb: 'all'
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RegionRoutingModule {

}
