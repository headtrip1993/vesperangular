export interface TicketDto {
  ticketsId: number
  detailId: number
  denomination: number
  quantity: number
}
